$(document).ready(function(){
	var userMenuButton = document.getElementsByClassName("layout-topbar-user");
	var userMenu = document.getElementsByClassName("layout-topbar-usermenu");
	 
	userMenuButton.onclick = function() {
		userMenu.addClass('layout-topbar-usermenu-active fadeInDown');
        e.preventDefault();
    };
});

function unselectNode(){
	var selectedNode = document.getElementById("form:organigram").getElementsByClassName("selected")[0];
	
	if(selectedNode){
		selectedNode.classList.remove("selected");
	}
}

var wrapper = $(document.body).children('.layout-wrapper');
    var topbar = this.wrapper.children('.layout-topbar');
    var menuButton = this.topbar.children('.layout-menu-button');
    var userMenuButton = this.topbar.children('.layout-topbar-user');
    var userMenu = this.topbar.children('.layout-topbar-usermenu');
    var menuContainer = this.wrapper.children('.layout-menu-container');
    var menu = this.jq;
    var menulinks = this.menu.find('a');
    var nanoContainer = this.menuContainer.find('.nano');

    var isSlimMenu = this.wrapper.hasClass('layout-slim');
    var isHorizontalMenu = this.wrapper.hasClass('layout-horizontal');

    if(!(isSlimMenu && this.isDesktop()) && !(isHorizontalMenu && this.isDesktop())) {
        this.restoreMenuState();
    }
    
    if(!isSlimMenu && !isHorizontalMenu) {
        this.nanoContainer.nanoScroller({flash:true});
        this.nanoContainer.children('.nano-content').removeAttr('tabindex');
    }

    $this.menu.on('click', function() {
        $this.menuClick = true;
    });

    this.menuButton.off('click').on('click', function(e) {
        $this.menuClick = true;

        if($this.isMobile()) {
            $this.wrapper.toggleClass('layout-mobile-active');
        }
        else {
            if($this.isStaticMenu()) {
                $this.wrapper.toggleClass('layout-static-inactive');
                $this.saveStaticMenuState();
            }
            else {
                $this.wrapper.toggleClass('layout-overlay-active');
            }               
        }

        e.preventDefault();
    });

    this.userMenuButton.off('click').on('click', function(e) {
        $this.userMenuClick = true;

        if ($this.userMenu.hasClass('layout-topbar-usermenu-active'))
            $this.hideUserMenu();
        else
            $this.userMenu.addClass('layout-topbar-usermenu-active fadeInDown');

        e.preventDefault();
    });

    this.menulinks.on('click', function (e) {
        var link = $(this),
        item = link.parent('li'),
        submenu = item.children('ul');

        if (item.hasClass('active-menuitem')) {
            if (submenu.length) {
                $this.removeMenuitem(item.attr('id'));
                
                if($this.isSlimMenu() || $this.isHorizontalMenu()) {
                    item.removeClass('active-menuitem');
                    submenu.hide();
                }
                else {
                    submenu.slideUp(400, function() {
                        item.removeClass('active-menuitem');
                    });
                }
            }

            if(item.parent().is($this.jq)) {
                $this.menuActive = false;
            }
        }
        else {
            $this.addMenuitem(item.attr('id'));

            if($this.isSlimMenu() || $this.isHorizontalMenu()) {
                $this.deactivateItems(item.siblings(), false);

                if(submenu.length === 0) {
                    $this.resetMenu();
                }
            }
            else {
                $this.deactivateItems(item.siblings(), true);
            }
            
            $this.activate(item);
            
            if(item.parent().is($this.jq)) {
                $this.menuActive = true;
            }
        }

        if(!$this.wrapper.hasClass('layout-slim') && !$this.wrapper.hasClass('layout-horizontal')) {
            setTimeout(function() {
                $this.nanoContainer.nanoScroller();
            }, 450);
        }

        if (submenu.length) {
            e.preventDefault();
        }
    });

    this.menu.children('li').on('mouseenter', function(e) {    
        if($this.isHorizontalMenu() || $this.isSlimMenu()) {
            var item = $(this);
            
            if(!item.hasClass('active-menuitem')) {
                $this.menu.find('.active-menuitem').removeClass('active-menuitem');
                $this.menu.find('ul:visible').hide();
                
                if($this.menuActive) {
                    item.addClass('active-menuitem');
                    item.children('ul').show();
                }
            }
        }
    });

    $(document.body).on('click', function() {
        if(($this.isHorizontalMenu() || $this.isSlimMenu()) && !$this.menuClick && $this.isDesktop()) {
            $this.menu.find('.active-menuitem').removeClass('active-menuitem');
            $this.menu.find('ul:visible').hide();
            $this.menuActive = false;
        }

        if(!$this.menuClick) {
            $this.wrapper.removeClass('layout-overlay-active layout-mobile-active');
        }

        if (!$this.userMenuClick && $this.userMenu.hasClass('layout-topbar-usermenu-active')) {
            $this.hideUserMenu();
        }

        $this.menuClick = false;
        $this.userMenuClick = false;
    });

/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
	// AMD (Register as an anonymous module)
	define(['jquery'], factory);
} else if (typeof exports === 'object') {
	// Node/CommonJS
	module.exports = factory(require('jquery'));
} else {
	// Browser globals
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

function encode(s) {
	return config.raw ? s : encodeURIComponent(s);
}

function decode(s) {
	return config.raw ? s : decodeURIComponent(s);
}

function stringifyCookieValue(value) {
	return encode(config.json ? JSON.stringify(value) : String(value));
}

function parseCookieValue(s) {
	if (s.indexOf('"') === 0) {
		// This is a quoted cookie as according to RFC2068, unescape...
		s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
	}

	try {
		// Replace server-side written pluses with spaces.
		// If we can't decode the cookie, ignore it, it's unusable.
		// If we can't parse the cookie, ignore it, it's unusable.
		s = decodeURIComponent(s.replace(pluses, ' '));
		return config.json ? JSON.parse(s) : s;
	} catch(e) {}
}

function read(s, converter) {
	var value = config.raw ? s : parseCookieValue(s);
	return $.isFunction(converter) ? converter(value) : value;
}

var config = $.cookie = function (key, value, options) {

	// Write

	if (arguments.length > 1 && !$.isFunction(value)) {
		options = $.extend({}, config.defaults, options);

		if (typeof options.expires === 'number') {
			var days = options.expires, t = options.expires = new Date();
			t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
		}

		return (document.cookie = [
			encode(key), '=', stringifyCookieValue(value),
			options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
			options.path    ? '; path=' + options.path : '',
			options.domain  ? '; domain=' + options.domain : '',
			options.secure  ? '; secure' : ''
		].join(''));
	}

	// Read

	var result = key ? undefined : {},
		// To prevent the for loop in the first place assign an empty array
		// in case there are no cookies at all. Also prevents odd result when
		// calling $.cookie().
		cookies = document.cookie ? document.cookie.split('; ') : [],
		i = 0,
		l = cookies.length;

	for (; i < l; i++) {
		var parts = cookies[i].split('='),
			name = decode(parts.shift()),
			cookie = parts.join('=');

		if (key === name) {
			// If second argument (value) is a function it's a converter...
			result = read(cookie, value);
			break;
		}

		// Prevent storing a cookie that we couldn't decode.
		if (!key && (cookie = read(cookie)) !== undefined) {
			result[name] = cookie;
		}
	}

	return result;
};

config.defaults = {};

$.removeCookie = function (key, options) {
	// Must not alter options, thus extending a fresh object...
	$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));