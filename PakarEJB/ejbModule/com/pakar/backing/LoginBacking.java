package com.pakar.backing;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.pakar.eao.UserEao;
import com.pakar.entity.User;
import com.pakar.utils.PasswordHelper;
import com.pakar.utils.ValidationHelper;

@Named
@RequestScoped
public class LoginBacking extends BasicBacking {

	private static final long serialVersionUID = 4114733596666077398L;
	
	@EJB
	private UserEao userEao;
	
	@Inject
	private transient UserBacking user;
	
	private User login = new User();
	
	public User getLogin() {
		return login;
	}
	public void setLogin(User login) {
		this.login = login;
	}
	
	public String userLogin() {
		if (ValidationHelper.isNullOrEmpty(login.getEmail())) {
			messageHandler("Email Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return null;
		} else if (!ValidationHelper.emailFormat(login.getEmail())) {
			messageHandler("Format Email Tidak Sesuai", FacesMessage.SEVERITY_ERROR);
			return null;
		} else if (ValidationHelper.isNullOrEmpty(login.getPassword())) {
			messageHandler("Password Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return null;
		} else {
			login.setPassword(PasswordHelper.encrypt(login.getPassword(), login.getEmail()));
			User userOut = userEao.userLogin(login.getEmail(), login.getPassword());
			if (userOut.getResultSuccess() == RESULT_SUCCESS || userOut.getResultSuccess() == RESULT_WARN) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.getExternalContext().getSessionMap().put("user", login.getEmail());
				user.getUserAllData(login.getEmail());
				messageHandler(userOut.getErrorMessage(), getSeverity(userOut.getResultSuccess()));
				flashMessages();
				return "index.xhtml?faces-redirect=true";
			} else {
				messageHandler(userOut.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
				return null;
			}
		}
	}
	
	public String userLogout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "login.xhtml?faces-redirect=true";
	}
}