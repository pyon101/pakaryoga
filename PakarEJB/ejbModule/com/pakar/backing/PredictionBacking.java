package com.pakar.backing;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import com.pakar.eao.DatasetEao;
import com.pakar.eao.PatientEao;
import com.pakar.eao.PredictionEao;
import com.pakar.entity.Patient;
import com.pakar.entity.Prediction;

@Named
@SessionScoped
public class PredictionBacking extends BasicBacking {
	
	private static final long serialVersionUID = 5317871584248556713L;

	@Inject
	private transient UserBacking user;
	
	@EJB
	private PredictionEao predicEao;
	
	@EJB
	private DatasetEao datasetEao;
	
	@EJB
	PatientEao patientEao;
	
	private Prediction predic = new Prediction();
	private Patient patient = new Patient();
	private Patient patientSearch = new Patient();
	private List<Patient> listPatient = new ArrayList<Patient>();
	private List<Prediction> listDataset = new ArrayList<Prediction>();
	private boolean predictionPanel, resultPrediction;
	

	public Prediction getPredic() {
		return predic;
	}

	
	
	public List<Prediction> getListDataset() {
		return listDataset;
	}



	public void setListDataset(List<Prediction> listDataset) {
		this.listDataset = listDataset;
	}



	public boolean isResultPrediction() {
		return resultPrediction;
	}



	public void setResultPrediction(boolean resultPrediction) {
		this.resultPrediction = resultPrediction;
	}



	public boolean isPredictionPanel() {
		return predictionPanel;
	}

	public void setPredictionPanel(boolean predictionPanel) {
		this.predictionPanel = predictionPanel;
	}

	
	public Patient getPatientSearch() {
		return patientSearch;
	}

	public void setPatientSearch(Patient patientSearch) {
		this.patientSearch = patientSearch;
	}

	public List<Patient> getListPatient() {
		return listPatient;
	}

	public void setListPatient(List<Patient> listPatient) {
		this.listPatient = listPatient;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public void setPredic(Prediction predic) {
		this.predic = predic;
	}
	
	//=============================================================================
	@PostConstruct
	public void init() {
		setPredictionPanel(false);
		setResultPrediction(false);
	}
	
	public void predictionPatient() {
		
		if(patient.getPatientID()==0) {
			messageHandler("Pasien Harus di pilih", FacesMessage.SEVERITY_ERROR);
		}else {
			if(predic.getFgActiveSmoker()==0) {
				predic.setCigarettesPerDay(0);
			}
			
			predic.setFgWeight(patient.getPatientWeightBMI());
			predic.setAge(patient.getPatientAge());
			predic.setFgGender(patient.getFgGender());
			predic = predicEao.PredictionPatient(patient.getPatientID(), predic, user.getUserAccount());
			if (predic.getResultSuccess() == RESULT_SUCCESS || predic.getResultSuccess() == RESULT_WARN) {
				messageHandler(predic.getErrorMessage(), getSeverity(predic.getResultSuccess()));
				setPredictionPanel(false);
				setResultPrediction(true);
				PFAjax().update(":form:panelPrediction");
				if(predic.getPredictionSuccess()==predic.getPredictionResult()) {
					predic.setResultText("Mengalami Penyempitan lebih dari 50%");
				}else {
					predic.setResultText("Mengalami Penyempitan kurang dari 50%");
				}
				System.out.println("getPredictionFailed = "+ predic.getPredictionFailed());
				System.out.println("getPredictionSuccess = "+ predic.getPredictionSuccess());
				System.out.println("getPredictionResult = "+ predic.getPredictionResult());
				System.out.println("getPredictionFgVasoconstriction = "+ predic.getPredictionFgVasoconstriction());
				
				
			} else {
				messageHandler(predic.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
			}
		}
		
	}
	
	public void loadListPatient() {
		patientSearch = new Patient();
		patient = new Patient();
		listPatient = patientEao.getPatient();
		setPredictionPanel(false);
		setResultPrediction(false);
		
	}
	public void loadDataPatient() {
		patient = patientEao.getPatientAllData(patientSearch.getPatientID());
		patient.setPatientID(patientSearch.getPatientID());
		System.out.println("Muncul");
		setPredictionPanel(true);
		setResultPrediction(false);
		PFAjax().update(":form:panelPrediction");
		
	}
	
	public void predictionDelete(int predictionID) {
		Prediction pre = predicEao.datasetDelete(predictionID, user.getUserAccount());
		if (pre.getResultSuccess() == RESULT_SUCCESS || pre.getResultSuccess() == RESULT_WARN) {
			messageHandler(pre.getErrorMessage(), getSeverity(pre.getResultSuccess()));
			loadListDataset();
		} else {
			messageHandler(pre.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
		}
	}
	public void loadListDataset() {
		listDataset = predicEao.getPrediction();
		int i = 1;
		for (Prediction ds : listDataset) {
			ds.setRowNumber(i++);
		}
		PFAjax().update("form:tbPredic");
	}

	public String returnPatientPrediction() {
		System.out.println("PREDIKSI");
		return "prediction.xhtml?faces-redirect=true";
	}
}