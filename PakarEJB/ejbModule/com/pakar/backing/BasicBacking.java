package com.pakar.backing;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;

import org.primefaces.PrimeFaces;
import org.primefaces.PrimeFaces.Ajax;

public class BasicBacking implements Serializable {
	
	private static final long serialVersionUID = -2287939902892443302L;

	protected static final int RESULT_FAIL = 0;
	protected static final int RESULT_SUCCESS = 1;
	protected static final int RESULT_WARN = 2;
	
	public FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}
	
	public ExternalContext getExternalContext() {
		return getFacesContext().getExternalContext();
	}
	
	public Flash getFlash() {
		return getExternalContext().getFlash();
	}
	
	public void clearMessages() {
		if (this.getFacesContext().getMessageList().size()>0) {
			this.getFacesContext().getMessageList().clear();
		}
	}
	
	public void flashMessages() {
		getFlash().setKeepMessages(true);
		getFlash().setRedirect(true);
	}
	
	public void messageHandler(String errorMessage, FacesMessage.Severity severity) {
		FacesMessage message = new FacesMessage(errorMessage);
		message.setSeverity(severity);
		this.getFacesContext().addMessage(null, message);
	}
	
	public Severity getSeverity(int ResultSuccess) {
		if (ResultSuccess == 1) {
			return FacesMessage.SEVERITY_INFO;
		} else if (ResultSuccess == 2) {
			return FacesMessage.SEVERITY_WARN;
		}
		return FacesMessage.SEVERITY_ERROR;
	}
	
	public Ajax PFAjax() {
		return PrimeFaces.current().ajax();
	}
}