package com.pakar.backing;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

import com.pakar.eao.UserEao;
import com.pakar.entity.User;
import com.pakar.utils.PasswordHelper;
import com.pakar.utils.ValidationHelper;

@Named
@SessionScoped
public class UserBacking extends BasicBacking {

	private static final long serialVersionUID = 4114733596666077398L;
	
	@EJB
	private UserEao userEao;
	
	private User user;
	private User change = new User();
	
	private Boolean changeDialog;
	
	public void getUserAllData(String Email) {
		user = userEao.getUserAllData(Email);
		user.setEmail(Email);
	}
	
	public String getUserAccount() {
		return user.getEmail();
	}
	
	public String getUseName() {
		return user.getName();
	}
	
	public String getUserMobilePhone() {
		return user.getMobilePhone();
	}
	
	public User getChange() {
		return change;
	}
	public void setChange(User change) {
		this.change = change;
	}

	public Boolean getChangeDialog() {
		return changeDialog;
	}

	public void setChangeDialog(Boolean changeDialog) {
		this.changeDialog = changeDialog;
	}
	
	public String userChangePassword() {
		System.out.println("Wumbo");
		if (ValidationHelper.isNullOrEmpty(change.getCurrentpassword())) {
			messageHandler("Password Lama Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return null;
		} else if (ValidationHelper.isNullOrEmpty(change.getPassword())) {
			messageHandler("Password Baru Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return null;
		} else if (ValidationHelper.isNullOrEmpty(change.getConfirmPassword())) {
			messageHandler("Konfirmasi Password Baru Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return null;
		} else if (!change.getPassword().equals(change.getConfirmPassword())) {
			messageHandler("Konfirmasi Password Baru Tidak Sesuai", FacesMessage.SEVERITY_ERROR);
			return null;
		} else {
			System.out.println(user.getEmail());
			change.setEmail(user.getEmail());
			change.setPassword(PasswordHelper.encrypt(change.getCurrentpassword(), change.getEmail()));
			User userOut = userEao.userLogin(change.getEmail(), change.getPassword());
			if (userOut.getResultSuccess() == RESULT_SUCCESS || userOut.getResultSuccess() == RESULT_WARN) {
				change.setConfirmPassword(PasswordHelper.encrypt(change.getConfirmPassword(), change.getEmail()));
				User userOut2 = userEao.userChangePassword(change.getEmail(), change.getConfirmPassword());
				if (userOut2.getResultSuccess() == RESULT_SUCCESS || userOut2.getResultSuccess() == RESULT_WARN) {
					messageHandler(userOut2.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
					closeChangeDialog();
					return null;
				} else {
					messageHandler(userOut2.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
					return null;
				}
			} else {
				messageHandler("Password Lama Tidak Sesuai", FacesMessage.SEVERITY_ERROR);
				return null;
			}
		}
	}
	
	public void openChangeDialog() {
		setChangeDialog(true);
		PFAjax().update("form:changeDialog");
	}
	
	public void closeChangeDialog() {
		clearChange();
		setChangeDialog(false);
		PFAjax().update("form:changeDialog");
	}
	
	private void clearChange() {
		change = new User();
	}
}