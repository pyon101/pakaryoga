package com.pakar.backing;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import com.pakar.eao.DatasetEao;
import com.pakar.entity.Dataset;

@Named
@SessionScoped
public class DatasetBacking extends BasicBacking {

	private static final long serialVersionUID = 4114733596666077398L;
	
	@Inject
	private transient UserBacking user;
	
	@EJB
	private DatasetEao datasetEao;
	
	private List<Dataset> listDataset = new ArrayList<Dataset>();
	
	private Dataset datasetAdd = new Dataset();
	private Dataset datasetEdit = new Dataset();
	
	private boolean addDialog, editDialog;
	
	public List<Dataset> getListDataset() {
		return listDataset;
	}
	public void setListDataset(List<Dataset> listDataset) {
		this.listDataset = listDataset;
	}
	public Dataset getDatasetAdd() {
		return datasetAdd;
	}
	public void setDatasetAdd(Dataset datasetAdd) {
		this.datasetAdd = datasetAdd;
	}
	public Dataset getDatasetEdit() {
		return datasetEdit;
	}
	public void setDatasetEdit(Dataset datasetEdit) {
		this.datasetEdit = datasetEdit;
	}
	public boolean isAddDialog() {
		return addDialog;
	}
	public void setAddDialog(boolean addDialog) {
		this.addDialog = addDialog;
	}
	public boolean isEditDialog() {
		return editDialog;
	}
	public void setEditDialog(boolean editDialog) {
		this.editDialog = editDialog;
	}
	
	public void loadListDataset() {
		listDataset = datasetEao.getDataset();
		int i = 1;
		for (Dataset ds : listDataset) {
			ds.setRowNumber(i++);
		}
		PFAjax().update("form:tbDataset");
	}
	
	public void dataset_Add() {
		Dataset datasetOut = datasetEao.datasetAdd(datasetAdd, user.getUserAccount());
		if (datasetOut.getResultSuccess() == RESULT_SUCCESS || datasetOut.getResultSuccess() == RESULT_WARN) {
			messageHandler(datasetOut.getErrorMessage(), getSeverity(datasetOut.getResultSuccess()));
			closeAddDialog();
			loadListDataset();
		} else {
			messageHandler(datasetOut.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
		}
	}
	
	public void dataset_Edit() {
		Dataset datasetOut = datasetEao.datasetEdit(datasetEdit, user.getUserAccount());
		if (datasetOut.getResultSuccess() == RESULT_SUCCESS || datasetOut.getResultSuccess() == RESULT_WARN) {
			messageHandler(datasetOut.getErrorMessage(), getSeverity(datasetOut.getResultSuccess()));
			closeEditDialog();
			loadListDataset();
		} else {
			messageHandler(datasetOut.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
		}
	}
	
	public void dataset_Delete(int DatasetID) {
		Dataset datasetOut = datasetEao.datasetDelete(DatasetID, user.getUserAccount());
		if (datasetOut.getResultSuccess() == RESULT_SUCCESS || datasetOut.getResultSuccess() == RESULT_WARN) {
			messageHandler(datasetOut.getErrorMessage(), getSeverity(datasetOut.getResultSuccess()));
			loadListDataset();
		} else {
			messageHandler(datasetOut.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
		}
	}
	
	public void openAddDialog() {
		setAddDialog(true);
		PFAjax().update("form:addDialog");
	}
	
	public void closeAddDialog() {
		clearDatasetAdd();
		setAddDialog(false);
		PFAjax().update("form:addDialog");
	}
	
	public void openEditDialog(Dataset dataset) {
		setDatasetEdit(dataset);
		setEditDialog(true);
		PFAjax().update("form:editDialog");
	}
	
	public void closeEditDialog() {
		clearDatasetEdit();
		setEditDialog(false);
		PFAjax().update("form:editDialog");
	}
	
	private void clearDatasetAdd() {
		datasetAdd = new Dataset();
	}
	
	private void clearDatasetEdit() {
		datasetEdit = new Dataset();
	}
}