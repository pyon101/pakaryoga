package com.pakar.backing;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.primefaces.model.chart.PieChartModel;

import com.pakar.eao.DashboardEao;
import com.pakar.eao.DatasetEao;
import com.pakar.eao.PredictionEao;
import com.pakar.entity.Dashboard;
import com.pakar.entity.Dataset;
import com.pakar.entity.Prediction;

@Named
@SessionScoped
public class DashboardBacking extends BasicBacking {

	private static final long serialVersionUID = 7817922585760552772L;
	
	@EJB
	private DashboardEao dashboardEao;
	
	@EJB
	private PredictionEao predicEao;
	
	@EJB
	private DatasetEao datasetEao;
	
	private Dashboard dashboard = new Dashboard();
	private PieChartModel pieModel;
	
	public Dashboard getDashboard() {
		return dashboard;
	}
	public void setDashboard(Dashboard dashboard) {
		this.dashboard = dashboard;
	}
	public PieChartModel getPieModel() {
		return pieModel;
	}
	public void setPieModel(PieChartModel pieModel) {
		this.pieModel = pieModel;
	}
	
	public void loadDashboard() {
		dashboard = dashboardEao.getDashboard();
		
		List<Dataset> listDataset = new ArrayList<Dataset>();
		List<Prediction> listPredictionRecord = new ArrayList<Prediction>();
		listDataset = datasetEao.getDataset();
		for (Dataset ds : listDataset) {
			listPredictionRecord.add(predicEao.predictionRecord(ds));
		}
		
		int totalSuccess = 0;
		int totalFailed = 0;
		int totalRecord = listDataset.size();
		for (Prediction pd : listPredictionRecord) {
			if (pd.getPredictionDataset() == pd.getPredictionNB()) {
				totalSuccess+=1;
			} else {
				totalFailed+=1;
			}
		}
		float divideSuccess = (float) totalSuccess /(float)  totalRecord;
		float divideFailed = (float) totalFailed / (float) totalRecord;
		
		float roundSuccess = (float) (Math.round(divideSuccess * 100d) / 100d);
		float roundFailed = (float) (Math.round(divideFailed * 100d) / 100d);
		
		float percentageSuccess = roundSuccess * 100;
		float percentageFailed = roundFailed * 100;
		
		pieModel = new PieChartModel();
		pieModel.set("Berhasil", percentageSuccess);
		pieModel.set("Tidak Berhasil", percentageFailed);
 
		pieModel.setTitle("Persentase Berhasil");
		pieModel.setLegendPosition("e");
		pieModel.setFill(false);
		pieModel.setShowDataLabels(true);
		pieModel.setDiameter(150);
		pieModel.setShadow(false);
	}
}