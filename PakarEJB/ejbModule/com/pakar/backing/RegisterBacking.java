package com.pakar.backing;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

import com.pakar.eao.UserEao;
import com.pakar.entity.User;
import com.pakar.utils.PasswordHelper;
import com.pakar.utils.ValidationHelper;

@Named
@SessionScoped
public class RegisterBacking extends BasicBacking {

	private static final long serialVersionUID = 4114733596666077398L;
	
	@EJB
	private UserEao userEao;
	
	private User register = new User();
	
	private boolean registerDialog;
	
	public User getRegister() {
		return register;
	}
	public void setRegister(User register) {
		this.register = register;
	}
	public boolean isRegisterDialog() {
		return registerDialog;
	}
	public void setRegisterDialog(boolean registerDialog) {
		this.registerDialog = registerDialog;
	}
	
	public void userRegister() {
		if (ValidationHelper.isNullOrEmpty(register.getName())) {
			messageHandler("Nama Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (ValidationHelper.isNullOrEmpty(register.getEmail())) {
			messageHandler("Email Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (!ValidationHelper.emailFormat(register.getEmail())) {
			messageHandler("Format Email Tidak Sesuai", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (ValidationHelper.isNullOrEmpty(register.getMobilePhone())) {
			messageHandler("Nomor Handphone Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (ValidationHelper.isNullOrEmpty(register.getPassword())) {
			messageHandler("Password Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (ValidationHelper.isNullOrEmpty(register.getConfirmPassword())) {
			messageHandler("Konfirmasi Password Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (!register.getPassword().equals(register.getConfirmPassword())) {
			messageHandler("Konfirmasi Password Tidak Sesuai", FacesMessage.SEVERITY_ERROR);
			return;
		} else {
			register.setConfirmPassword(PasswordHelper.encrypt(register.getConfirmPassword(), register.getEmail()));
			User userOut = userEao.userRegister(register);
			if (userOut.getResultSuccess() == RESULT_SUCCESS || userOut.getResultSuccess() == RESULT_WARN) {
				messageHandler(userOut.getErrorMessage(), getSeverity(userOut.getResultSuccess()));
				closeRegisterDialog();
			} else {
				messageHandler(userOut.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
			}
		}
	}
	
	public void openRegisterDialog() {
		setRegisterDialog(true);
		PFAjax().update("form:registerDialog");
	}
	
	public void closeRegisterDialog() {
		clearRegister();
		setRegisterDialog(false);
		PFAjax().update("form:registerDialog");
	}
	
	private void clearRegister() {
		register = new User();
	}
}