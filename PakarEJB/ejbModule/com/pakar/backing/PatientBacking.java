package com.pakar.backing;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import com.pakar.eao.PatientEao;
import com.pakar.entity.Patient;
import com.pakar.entity.Prediction;
import com.pakar.utils.ValidationHelper;

@Named
@SessionScoped
public class PatientBacking extends BasicBacking {

	private static final long serialVersionUID = 4114733596666077398L;
	
	@Inject
	private transient UserBacking user;
	
	@Inject
	private transient DiseaseBacking diseaseBacking;
	
	@EJB
	private PatientEao patientEao;
	
	private List<Patient> listPatient = new ArrayList<Patient>();
	private List<Prediction> listPredict = new ArrayList<Prediction>();
	
	private Patient patientAdd = new Patient();
	private Patient patientEdit = new Patient();
	private Patient patientDisease = new Patient();
	
	private boolean addDialog, editDialog, diseaseDialog;
	
	public List<Patient> getListPatient() {
		return listPatient;
	}
	public void setListPatient(List<Patient> listPatient) {
		this.listPatient = listPatient;
	}
	public Patient getPatientAdd() {
		return patientAdd;
	}
	public void setPatientAdd(Patient patientAdd) {
		this.patientAdd = patientAdd;
	}
	public Patient getPatientEdit() {
		return patientEdit;
	}
	public void setPatientEdit(Patient patientEdit) {
		this.patientEdit = patientEdit;
	}
	public Patient getPatientDisease() {
		return patientDisease;
	}
	public void setPatientDisease(Patient patientDisease) {
		this.patientDisease = patientDisease;
	}
	public boolean isAddDialog() {
		return addDialog;
	}
	public void setAddDialog(boolean addDialog) {
		this.addDialog = addDialog;
	}
	public boolean isEditDialog() {
		return editDialog;
	}
	public void setEditDialog(boolean editDialog) {
		this.editDialog = editDialog;
	}
	public boolean isDiseaseDialog() {
		return diseaseDialog;
	}
	public void setDiseaseDialog(boolean diseaseDialog) {
		this.diseaseDialog = diseaseDialog;
	}
	public List<Prediction> getListPredict() {
		return listPredict;
	}
	public void setListPredict(List<Prediction> listPredict) {
		this.listPredict = listPredict;
	}
	
	public void loadListPatient() {
		listPatient = patientEao.getPatient();
		PFAjax().update("form:tbPatient");
	}
	
	public void patient_Add() {
		if (ValidationHelper.isNullOrEmpty(patientAdd.getPatientName())) {
			messageHandler("Nama Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (patientAdd.getPatientWeight() == 0) {
			messageHandler("Berat Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (patientAdd.getPatientHeight() == 0) {
			messageHandler("Tinggi Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (patientAdd.getBirthOfDate() == null) {
			messageHandler("Tanggal Lahir Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (ValidationHelper.isNullOrEmpty(patientAdd.getMobilePhone())) {
			messageHandler("Nomor Handphone Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (ValidationHelper.isNullOrEmpty(patientAdd.getAddress())) {
			messageHandler("Alamat Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else {
			patientAdd.setPatientHeightMeter((float) patientAdd.getPatientHeight() / 100);
			patientAdd.setPatientHeightMeter(patientAdd.getPatientHeightMeter() * patientAdd.getPatientHeightMeter());
			patientAdd.setPatientWeightBMI((float)patientAdd.getPatientWeight() / patientAdd.getPatientHeightMeter());
			Patient patientOut = patientEao.patientAdd(patientAdd, user.getUserAccount());
			if (patientOut.getResultSuccess() == RESULT_SUCCESS || patientOut.getResultSuccess() == RESULT_WARN) {
				messageHandler(patientOut.getErrorMessage(), getSeverity(patientOut.getResultSuccess()));
				closeAddDialog();
				loadListPatient();
				openDiseaseDialog(patientOut.getPatientID());
			} else {
				messageHandler(patientOut.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
			}
		}
	}
	
	public void patient_Edit() {
		if (ValidationHelper.isNullOrEmpty(patientEdit.getPatientName())) {
			messageHandler("Nama Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (patientEdit.getPatientWeight() == 0) {
			messageHandler("Berat Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (patientEdit.getPatientHeight() == 0) {
			messageHandler("Tinggi Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (patientEdit.getBirthOfDate() == null) {
			messageHandler("Tanggal Lahir Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (ValidationHelper.isNullOrEmpty(patientEdit.getMobilePhone())) {
			messageHandler("Nomor Handphone Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (ValidationHelper.isNullOrEmpty(patientEdit.getAddress())) {
			messageHandler("Alamat Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else {
			patientEdit.setPatientHeightMeter((float) patientEdit.getPatientHeight() / 100);
			patientEdit.setPatientHeightMeter(patientEdit.getPatientHeightMeter() * patientEdit.getPatientHeightMeter());
			patientEdit.setPatientWeightBMI((float)patientEdit.getPatientWeight() / patientEdit.getPatientHeightMeter());
			Patient patientOut = patientEao.patientEdit(patientEdit, user.getUserAccount());
			if (patientOut.getResultSuccess() == RESULT_SUCCESS || patientOut.getResultSuccess() == RESULT_WARN) {
				messageHandler(patientOut.getErrorMessage(), getSeverity(patientOut.getResultSuccess()));
				closeEditDialog();
				loadListPatient();
			} else {
				messageHandler(patientOut.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
			}
		}
	}
	
	public void patient_Delete(int PatientID) {
		Patient patientOut = patientEao.patientDelete(PatientID, user.getUserAccount());
		if (patientOut.getResultSuccess() == RESULT_SUCCESS || patientOut.getResultSuccess() == RESULT_WARN) {
			messageHandler(patientOut.getErrorMessage(), getSeverity(patientOut.getResultSuccess()));
			loadListPatient();
		} else {
			messageHandler(patientOut.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
		}
	}
	
	public String diseaseAddFromPatient() {
		closeDiseaseDialog();
		return diseaseBacking.diseaseAddFromPatient(patientDisease.getPatientID());
	}
	
	public void openAddDialog() {
		setAddDialog(true);
		PFAjax().update("form:addDialog");
	}
	
	public void closeAddDialog() {
		clearPatientAdd();
		setAddDialog(false);
		PFAjax().update("form:addDialog");
	}
	
	public void openEditDialog(Patient patient) {
		setPatientEdit(patient);
		setEditDialog(true);
		PFAjax().update("form:editDialog");
	}
	
	public void closeEditDialog() {
		clearPatientEdit();
		setEditDialog(false);
		PFAjax().update("form:editDialog");
	}
	
	public void openDiseaseDialog(int PatientID) {
		patientDisease.setPatientID(PatientID);
		setDiseaseDialog(true);
		PFAjax().update("form:diseaseDialog");
	}
	
	public void closeDiseaseDialog() {
		setDiseaseDialog(false);
		PFAjax().update("form:diseaseDialog");
	}
	
	private void clearPatientAdd() {
		patientAdd = new Patient();
	}
	
	private void clearPatientEdit() {
		patientEdit = new Patient();
	}
}