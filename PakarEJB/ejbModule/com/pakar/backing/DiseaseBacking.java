package com.pakar.backing;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import com.pakar.eao.DiseaseEao;
import com.pakar.eao.PatientEao;
import com.pakar.eao.PredictionEao;
import com.pakar.entity.Disease;
import com.pakar.entity.Patient;
import com.pakar.entity.Prediction;
import com.pakar.utils.ValidationHelper;

@Named
@SessionScoped
public class DiseaseBacking extends BasicBacking {

	private static final long serialVersionUID = 4114733596666077398L;
	
	@Inject
	private transient UserBacking user;
	
	@EJB
	private DiseaseEao diseaseEao;
	
	@EJB
	PatientEao patientEao;
	
	@EJB
	private PredictionEao predictionEao;
	
	private List<Patient> listPatient = new ArrayList<Patient>();
	
	private List<Disease> listDisease = new ArrayList<Disease>();
	
	private Disease diseaseAdd = new Disease();
	private Disease diseaseEdit = new Disease();
	
	private Patient patientSearch = new Patient();
	private Patient patient = new Patient();
	private List<Prediction> listPredict = new ArrayList<Prediction>();
	private boolean addDialog, editDialog;
	
	private int fgLoadFrom;
	
	public int getFgLoadFrom() {
		return fgLoadFrom;
	}
	public void setFgLoadFrom(int fgLoadFrom) {
		this.fgLoadFrom = fgLoadFrom;
	}
	public List<Prediction> getListPredict() {
		return listPredict;
	}
	public void setListPredict(List<Prediction> listPredict) {
		this.listPredict = listPredict;
	}
	public List<Patient> getListPatient() {
		return listPatient;
	}
	public void setListPatient(List<Patient> listPatient) {
		this.listPatient = listPatient;
	}
	public List<Disease> getListDisease() {
		return listDisease;
	}
	public void setListDisease(List<Disease> listDisease) {
		this.listDisease = listDisease;
	}
	public Disease getDiseaseAdd() {
		return diseaseAdd;
	}
	public void setDiseaseAdd(Disease diseaseAdd) {
		this.diseaseAdd = diseaseAdd;
	}
	public Disease getDiseaseEdit() {
		return diseaseEdit;
	}
	public void setDiseaseEdit(Disease diseaseEdit) {
		this.diseaseEdit = diseaseEdit;
	}
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public Patient getPatientSearch() {
		return patientSearch;
	}
	public void setPatientSearch(Patient patientSearch) {
		this.patientSearch = patientSearch;
	}
	public boolean isAddDialog() {
		return addDialog;
	}
	public void setAddDialog(boolean addDialog) {
		this.addDialog = addDialog;
	}
	public boolean isEditDialog() {
		return editDialog;
	}
	public void setEditDialog(boolean editDialog) {
		this.editDialog = editDialog;
	}
	
	public void loadListDisease() {
		patient = patientEao.getPatientAllData(patientSearch.getPatientID());
		patient.setPatientID(patientSearch.getPatientID());
		listDisease = diseaseEao.getDisease(patient.getPatientID());
		listPredict = predictionEao.getPredictionByPatient(patient.getPatientID());
		PFAjax().update("form:tbDisease");
	}
	
	public void loadListPatient() {
		if (fgLoadFrom == 1) {
			listDisease = new ArrayList<Disease>();
			listPatient = patientEao.getPatient();
		} else {
			patientSearch = new Patient();
			patient = new Patient();
			listDisease = new ArrayList<Disease>();
			listPatient = patientEao.getPatient();
		}
		setFgLoadFrom(0);
	}
	
	public void disease_Add() {
		if (ValidationHelper.isNullOrEmpty(diseaseAdd.getDiseaseName())) {
			messageHandler("Penyakit Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (ValidationHelper.isNullOrEmpty(diseaseAdd.getDiseaseDesc())) {
			messageHandler("Keterangan Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else {
			Disease diseaseOut = diseaseEao.diseaseAdd(patient.getPatientID(), diseaseAdd, user.getUserAccount());
			if (diseaseOut.getResultSuccess() == RESULT_SUCCESS || diseaseOut.getResultSuccess() == RESULT_WARN) {
				messageHandler(diseaseOut.getErrorMessage(), getSeverity(diseaseOut.getResultSuccess()));
				closeAddDialog();
				loadListDisease();
			} else {
				messageHandler(diseaseOut.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
			}
		}
	}
	
	public void disease_Edit() {
		if (ValidationHelper.isNullOrEmpty(diseaseEdit.getDiseaseName())) {
			messageHandler("Penyakit Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else if (ValidationHelper.isNullOrEmpty(diseaseEdit.getDiseaseDesc())) {
			messageHandler("Keterangan Tidak Boleh Kosong", FacesMessage.SEVERITY_ERROR);
			return;
		} else {
			Disease diseaseOut = diseaseEao.diseaseEdit(diseaseEdit, user.getUserAccount());
			if (diseaseOut.getResultSuccess() == RESULT_SUCCESS || diseaseOut.getResultSuccess() == RESULT_WARN) {
				messageHandler(diseaseOut.getErrorMessage(), getSeverity(diseaseOut.getResultSuccess()));
				closeEditDialog();
				loadListDisease();
			} else {
				messageHandler(diseaseOut.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
			}
		}
	}
	
	public void disease_Delete(int DiseaseID) {
		Disease diseaseOut = diseaseEao.diseaseDelete(DiseaseID, user.getUserAccount());
		if (diseaseOut.getResultSuccess() == RESULT_SUCCESS || diseaseOut.getResultSuccess() == RESULT_WARN) {
			messageHandler(diseaseOut.getErrorMessage(), getSeverity(diseaseOut.getResultSuccess()));
			loadListDisease();
		} else {
			messageHandler(diseaseOut.getErrorMessage(), FacesMessage.SEVERITY_ERROR);
		}
	}
	
	public String diseaseAddFromPatient(int PatientID) {
		patientSearch.setPatientID(PatientID);
		openAddDialog();
		setFgLoadFrom(1);
		loadListDisease();
		return "disease.xhtml?faces-redirect=true";
	}
	
	public void openAddDialog() {
		setAddDialog(true);
		PFAjax().update("form:addDialog");
	}
	
	public void closeAddDialog() {
		clearDiseaseAdd();
		setAddDialog(false);
		PFAjax().update("form:addDialog");
	}
	
	public void openEditDialog(Disease disease) {
		setDiseaseEdit(disease);
		setEditDialog(true);
		PFAjax().update("form:editDialog");
	}
	
	public void closeEditDialog() {
		clearDiseaseEdit();
		setEditDialog(false);
		PFAjax().update("form:editDialog");
	}
	
	private void clearDiseaseAdd() {
		diseaseAdd = new Disease();
	}
	
	private void clearDiseaseEdit() {
		diseaseEdit = new Disease();
	}
	
	public void generatePdf(Prediction pred) {
		try {
			String path = "C:\\";
            //JFileChooser j = new JFileChooser();
            //j.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            //int x = j.showSaveDialog(this);
            //if(x==JFileChooser.APPROVE_OPTION){
              //  path = j.getSelectedFile().getPath();
                float top = 70f; 
                float bottom = 70f;
                float right = 70f;
                float left = 70f;
                
                        
                Document doc = new Document(PageSize.A4 , left, right, top, bottom);
                //doc.setMargins(20,20,20,20);
                String buka = path+"Laporan Hasil Prediksi Pasien"+pred.getPredictionID()+".pdf";
                 try {
                   PdfWriter.getInstance(doc, new FileOutputStream(buka));
                   
                   doc.open();
                    Paragraph param = new Paragraph("    "+"Parameter",FontFactory.getFont(FontFactory.HELVETICA_BOLD));
                    Paragraph hasil1 = new Paragraph("    "+"Hasil",FontFactory.getFont(FontFactory.HELVETICA_BOLD));
                    Table table = new Table(2);
                    
                    
                    table.addCell(new Cell(param));       
                    table.addCell(new Cell(hasil1));       
                    table.addCell(new Cell("    "+"Nama Pasien"));       
                    table.addCell(new Cell("    "+patient.getPatientName()+"\n"));       
                    table.addCell(new Cell("    "+"Jenis Kelamin"));       
                    table.addCell(new Cell("    "+patient.getFgGenderText()+"\n")); 
                    table.addCell(new Cell("    "+"Usia Pasien"));       
                    table.addCell(new Cell("    "+patient.getPatientAge()+"\n"));
                    table.addCell(new Cell("    "+"Index Berat Badan (BMI)"));       
                    table.addCell(new Cell("    "+patient.getPatientWeightBMI()+"\n"));
                    table.addCell(new Cell("    "+"Perokok Aktif"));       
                    table.addCell(new Cell("    "+pred.getFgActiveSmokerText()+"\n"));
                    table.addCell(new Cell("    "+"Jenis Benjolan"));       
                    table.addCell(new Cell("    "+pred.getCigarettesPerDay()+"\n"));
                    table.addCell(new Cell("    "+"Stroke"));       
                    table.addCell(new Cell("    "+pred.getFgStrokeText()+"\n"));
                    table.addCell(new Cell("    "+"Darah Tinggi"));       
                    table.addCell(new Cell("    "+pred.getFgHighBloodText()+"\n"));
                    table.addCell(new Cell("    "+"Diabetes"));       
                    table.addCell(new Cell("    "+pred.getFgDiabetesText()+"\n"));
                    table.addCell(new Cell("    "+"Kolesterol"));       
                    table.addCell(new Cell("    "+pred.getCholesterol()+"\n"));
                    table.addCell(new Cell("    "+"Sistolik"));       
                    table.addCell(new Cell("    "+pred.getSistolikRate()+"\n"));
                    table.addCell(new Cell("    "+"Diastolik"));       
                    table.addCell(new Cell("    "+pred.getDiastolikRate()+"\n"));
                    table.addCell(new Cell("    "+"Detak Jantung"));       
                    table.addCell(new Cell("    "+pred.getHeartRate()+"\n"));
                    
                    table.setWidth(80);
                   
                    table.setPadding(3f);
                   // table.setSpaceBetweenCells(10f);
                    
                    Paragraph judul = new Paragraph("Laporan Hasil Prediksi Penyempitan Pembuluh Darah Pada Jantung"+"\n\n" , FontFactory.getFont(FontFactory.HELVETICA_BOLD,24));
                    Paragraph tanggal1 = new Paragraph("Tanggal Prediksi :"+"\n" + pred.getPredictionDateText());
                    tanggal1.setAlignment(Paragraph.ALIGN_RIGHT);
                    judul.setAlignment(Paragraph.ALIGN_CENTER);
                    doc.add(judul);
                    doc.add(tanggal1);
                    doc.add(new Paragraph("\n"+"Berikut adalah data hasil prediksi pasien :"+"\n"));
                    doc.add(table);
                   
                    

                    
                    doc.add(new Paragraph("\n\n"+"Respon Perawatan ", FontFactory.getFont(FontFactory.HELVETICA_BOLD,14)));
                    String penyempitan = null;
                    if(pred.getFgVasoconstriction() == 1) {
                    	penyempitan = "Mengalami Penyempitan pembuluh darah jantung lebih dari 50%";
                    }else {
                    	penyempitan = "Mengalami Penyempitan pembuluh darah jantung kurang dari 50%";
                    }
                    doc.add(new Paragraph(penyempitan));
                    doc.close();
                    File fileName = new File(buka);
                    
                    try {

                		if (fileName.exists()) {

                			Process p = Runtime
                			   .getRuntime()
                			   .exec("rundll32 url.dll,FileProtocolHandler "+buka);
                			p.waitFor();

                		} else {

                			System.out.println("File is not exists");

                		}
                 	}catch (Exception e) {
						e.printStackTrace();// TODO: handle exception
					}
                 	}catch (Exception e) {
						e.printStackTrace();// TODO: handle exception
					}
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		
	}
}