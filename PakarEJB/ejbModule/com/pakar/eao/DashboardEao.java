package com.pakar.eao;

import java.sql.CallableStatement;
import java.sql.Connection;

import javax.ejb.Stateless;

import com.pakar.entity.Dashboard;
import com.pakar.utils.ReleaseConnection;
import com.pakar.utils.Settings;

@Stateless
public class DashboardEao {
	
	public Dashboard getDashboard() {
		Dashboard dashboard = new Dashboard();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_DashboardGet (?,?,?,?,?) }");
			cs.registerOutParameter(1, java.sql.Types.INTEGER);
			cs.registerOutParameter(2, java.sql.Types.INTEGER);
			cs.registerOutParameter(3, java.sql.Types.INTEGER);
			cs.registerOutParameter(4, java.sql.Types.INTEGER);
			cs.registerOutParameter(5, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			dashboard.setTotalDataset(cs.getInt(1));
			dashboard.setTotalPrediction(cs.getInt(2));
			dashboard.setTotalPatient(cs.getInt(3));
			dashboard.setResultSuccess(cs.getInt(4));
			dashboard.setErrorMessage(cs.getString(5));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return dashboard;
	}
}