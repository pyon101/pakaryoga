package com.pakar.eao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import com.pakar.entity.Patient;
import com.pakar.utils.DateHelper;
import com.pakar.utils.ReleaseConnection;
import com.pakar.utils.Settings;

@Stateless
public class PatientEao {
	
	public List<Patient> getPatient() {
    	List<Patient> listPatient = new ArrayList<Patient>();
    	Connection connection = null;
    	CallableStatement cs = null;
    	ResultSet rs = null;
    	try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_PatientGet}");
			rs = cs.executeQuery();
			while(rs.next()) {
				Patient patient = new Patient();
				patient.setPatientID(rs.getInt("PatientID"));
				patient.setPatientName(rs.getString("PatientName"));
				patient.setFgGender(rs.getInt("FgGender"));
				patient.setFgGenderText(rs.getString("FgGenderText"));
				patient.setPatientWeight(rs.getInt("PatientWeight"));
				patient.setPatientHeight(rs.getInt("PatientHeight"));
				patient.setPatientWeightBMI(rs.getFloat("PatientWeightBMI"));
				patient.setBirthOfDate(rs.getDate("BirthOfDate"));
				patient.setBirthOfDateView(DateHelper.formatDateToString(rs.getDate("BirthOfDate"), "dd MMM yyyy"));
				patient.setPhoneCode(rs.getString("PhoneCode"));
				patient.setMobilePhone(rs.getString("MobilePhone"));
				patient.setPatientAge(DateHelper.calculateAge(DateHelper.formatDateToLocalDate(DateHelper.formatStringToDate(rs.getString("BirthOfDate"), "yyyy-MM-dd")), LocalDate.now()));
				patient.setAddress(rs.getString("PatientAddress"));
				listPatient.add(patient);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs, rs);
		}
    	return listPatient;
    }
	
	public Patient getPatientAllData(int PatientID) {
		Patient patient = new Patient();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_PatientGetAllData (?,?,?,?,?,?,?,?,?,?,?,?,?) }");
			cs.setInt(1, PatientID);
			cs.registerOutParameter(2, java.sql.Types.VARCHAR);
			cs.registerOutParameter(3, java.sql.Types.INTEGER);
			cs.registerOutParameter(4, java.sql.Types.VARCHAR);
			cs.registerOutParameter(5, java.sql.Types.INTEGER);
			cs.registerOutParameter(6, java.sql.Types.INTEGER);
			cs.registerOutParameter(7, java.sql.Types.FLOAT);
			cs.registerOutParameter(8, java.sql.Types.VARCHAR);
			cs.registerOutParameter(9, java.sql.Types.VARCHAR);
			cs.registerOutParameter(10, java.sql.Types.VARCHAR);
			cs.registerOutParameter(11, java.sql.Types.DATE);
			cs.registerOutParameter(12, java.sql.Types.INTEGER);
			cs.registerOutParameter(13, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			patient.setPatientName(cs.getString(2));
			patient.setFgGender(cs.getInt(3));
			patient.setFgGenderText(cs.getString(4));
			patient.setPatientWeight(cs.getInt(5));
			patient.setPatientHeight(cs.getInt(6));
			patient.setPatientWeightBMI(cs.getFloat(7));
			patient.setPhoneCode(cs.getString(8));
			patient.setMobilePhone(cs.getString(9));
			patient.setAddress(cs.getString(10));
			patient.setBirthOfDate(cs.getDate(11));
			patient.setBirthOfDateView(DateHelper.formatDateToString(cs.getDate(11), "dd MMM yyyy"));
			patient.setPatientAge(DateHelper.calculateAge(DateHelper.formatDateToLocalDate(DateHelper.formatStringToDate(cs.getString(11), "yyyy-MM-dd")), LocalDate.now()));
			patient.setResultSuccess(cs.getInt(12));
			patient.setErrorMessage(cs.getString(13));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return patient;
	}
	
	public Patient patientAdd(Patient Ptt, String User) {
		System.out.println(Ptt.getPatientName() + " | " + Ptt.getFgGender() + " | " + Ptt.getPatientWeight() + " | " + Ptt.getPatientHeight() + " | " + Ptt.getPatientWeightBMI() + " | " + DateHelper.formatDateToString(Ptt.getBirthOfDate(), "yyyy-MM-dd") + " | " + Ptt.getMobilePhone() + " | " + Ptt.getAddress() + " | " + User);
		Patient patient = new Patient();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_PatientAdd (?,?,?,?,?,?,?,?,?,?,?,?) }");
			cs.setString(1, Ptt.getPatientName());
			cs.setInt(2, Ptt.getFgGender());
			cs.setInt(3, Ptt.getPatientWeight());
			cs.setInt(4, Ptt.getPatientHeight());
			cs.setFloat(5, Ptt.getPatientWeightBMI());
			cs.setString(6, DateHelper.formatDateToString(Ptt.getBirthOfDate(), "yyyy-MM-dd"));
			cs.setString(7, Ptt.getMobilePhone());
			cs.setString(8, Ptt.getAddress());
			cs.setString(9, User);
			cs.registerOutParameter(10, java.sql.Types.INTEGER);
			cs.registerOutParameter(11, java.sql.Types.INTEGER);
			cs.registerOutParameter(12, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			patient.setPatientID(cs.getInt(10));
			patient.setResultSuccess(cs.getInt(11));
			patient.setErrorMessage(cs.getString(12));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return patient;
	}
	
	public Patient patientEdit(Patient Ptt, String User) {
		Patient patient = new Patient();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_PatientEdit (?,?,?,?,?,?,?,?,?,?,?,?) }");
			cs.setInt(1, Ptt.getPatientID());
			cs.setString(2, Ptt.getPatientName());
			cs.setInt(3, Ptt.getFgGender());
			cs.setInt(4, Ptt.getPatientWeight());
			cs.setInt(5, Ptt.getPatientHeight());
			cs.setFloat(6, Ptt.getPatientWeightBMI());
			cs.setString(7, DateHelper.formatDateToString(Ptt.getBirthOfDate(), "yyyy-MM-dd"));
			cs.setString(8, Ptt.getMobilePhone());
			cs.setString(9, Ptt.getAddress());
			cs.setString(10, User);
			cs.registerOutParameter(11, java.sql.Types.INTEGER);
			cs.registerOutParameter(12, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			patient.setResultSuccess(cs.getInt(11));
			patient.setErrorMessage(cs.getString(12));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return patient;
	}
	
	public Patient patientDelete(int PatientID, String User) {
		Patient patient = new Patient();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_PatientDelete (?,?,?,?) }");
			cs.setInt(1, PatientID);
			cs.setString(2, User);
			cs.registerOutParameter(3, java.sql.Types.INTEGER);
			cs.registerOutParameter(4, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			patient.setResultSuccess(cs.getInt(3));
			patient.setErrorMessage(cs.getString(4));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return patient;
	}
}