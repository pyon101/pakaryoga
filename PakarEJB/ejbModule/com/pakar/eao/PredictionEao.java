package com.pakar.eao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import com.pakar.entity.Dataset;
import com.pakar.entity.Prediction;
import com.pakar.utils.DateHelper;
import com.pakar.utils.ReleaseConnection;
import com.pakar.utils.Settings;

@Stateless
public class PredictionEao {
	
	public Prediction PredictionPatient(int patientID,Prediction ds, String User) {
		Prediction pred = new Prediction();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_PredictionGet (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
			cs.setInt(1, patientID);
			cs.setInt(2, ds.getFgGender());
			cs.setInt(3, ds.getAge());
			cs.setInt(4, ds.getFgActiveSmoker());
			cs.setInt(5, ds.getCigarettesPerDay());
			cs.setInt(6, ds.getFgStroke());
			cs.setInt(7, ds.getFgHighBlood());
			cs.setInt(8, ds.getFgDiabetes());
			cs.setInt(9, ds.getCholesterol());
			cs.setFloat(10, ds.getSistolikRate());
			cs.setFloat(11, ds.getDiastolikRate());
			cs.setFloat(12, ds.getFgWeight());
			cs.setInt(13, ds.getHeartRate());
			cs.setString(14, User);
			cs.registerOutParameter(15, java.sql.Types.DOUBLE);
			cs.registerOutParameter(16, java.sql.Types.DOUBLE);
			cs.registerOutParameter(17, java.sql.Types.DOUBLE);
			cs.registerOutParameter(18, java.sql.Types.INTEGER);
			cs.registerOutParameter(19, java.sql.Types.INTEGER);
			cs.registerOutParameter(20, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			pred.setPredictionSuccess(cs.getDouble(15));
			pred.setPredictionFailed(cs.getDouble(16));
			pred.setPredictionResult(cs.getDouble(17));
			pred.setPredictionFgVasoconstriction(cs.getInt(18));
			pred.setResultSuccess(cs.getInt(19));
			pred.setErrorMessage(cs.getString(20));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return pred;
	}
	
	public Prediction predictionRecord(Dataset ds) {
		Prediction pred = new Prediction();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_PredictionRecord (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
			cs.setInt(1, ds.getDatasetID());
			cs.setInt(2, ds.getFgGender());
			cs.setInt(3, ds.getAge());
			cs.setInt(4, ds.getFgActiveSmoker());
			cs.setInt(5, ds.getCigarettesPerDay());
			cs.setInt(6, ds.getFgStroke());
			cs.setInt(7, ds.getFgHighBlood());
			cs.setInt(8, ds.getFgDiabetes());
			cs.setInt(9, ds.getCholesterol());
			cs.setFloat(10, ds.getSistolikRate());
			cs.setFloat(11, ds.getDiastolikRate());
			cs.setFloat(12, ds.getFgWeight());
			cs.setInt(13, ds.getHeartRate());
			cs.registerOutParameter(14, java.sql.Types.INTEGER);
			cs.registerOutParameter(15, java.sql.Types.INTEGER);
			cs.executeUpdate();
			pred.setPredictionDataset(cs.getInt(14));
			pred.setPredictionNB(cs.getInt(15));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return pred;
	}
	
	public List<Prediction> getPrediction() {
    	List<Prediction> listDataset = new ArrayList<Prediction>();
    	Connection connection = null;
    	CallableStatement cs = null;
    	ResultSet rs = null;
    	try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_PredictionHistoryGet}");
			rs = cs.executeQuery();
			while(rs.next()) {
				Prediction pred = new Prediction();
				pred.setPredictionID(rs.getInt("PredictionID"));
				pred.setDatasetID(rs.getInt("DatasetID"));
				pred.setFgGender(rs.getInt("FgGender"));
				pred.setFgGenderText(rs.getString("FgGenderText"));
				pred.setAge(rs.getInt("Age"));
				pred.setAgeText(rs.getString("AgeText"));
				pred.setFgActiveSmoker(rs.getInt("FgActiveSmoker"));
				pred.setFgActiveSmokerText(rs.getString("FgActiveSmokerText"));
				pred.setCigarettesPerDay(rs.getInt("CigarettesPerDay"));
				pred.setCigarettesPerDayText(rs.getString("CigarettesPerDayText"));
				pred.setFgStroke(rs.getInt("FgStroke"));
				pred.setFgStrokeText(rs.getString("FgStrokeText"));
				pred.setFgHighBlood(rs.getInt("FgHighBlood"));
				pred.setFgHighBloodText(rs.getString("FgHighBloodText"));
				pred.setFgDiabetes(rs.getInt("FgDiabetes"));
				pred.setFgDiabetesText(rs.getString("FgDiabetesText"));
				pred.setCholesterol(rs.getInt("Cholesterol"));
				pred.setCholesterolText(rs.getString("CholesterolText"));
				pred.setSistolikRate(rs.getFloat("SistolikRate"));
				pred.setSistolikRateText(rs.getString("SistolikRateText"));
				pred.setDiastolikRate(rs.getFloat("DiastolikRate"));
				pred.setDiastolikRateText(rs.getString("DiastolikRateText"));
				pred.setFgWeight(rs.getFloat("FgWeight"));
				pred.setFgWeightText(rs.getString("FgWeightText"));
				pred.setHeartRate(rs.getInt("HeartRate"));
				pred.setHeartRateText(rs.getString("HeartRateText"));
				pred.setFgVasoconstriction(rs.getInt("FgVasoconstriction"));
				pred.setFgVasoconstrictionText(rs.getString("FgVasoconstrictionText"));
				listDataset.add(pred);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs, rs);
		}
    	return listDataset;
    }
	
	public Prediction datasetDelete(int predictionID, String User) {
		Prediction dataset = new Prediction();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_PredictionHistoryDelete (?,?,?,?) }");
			cs.setInt(1, predictionID);
			cs.setString(2, User);
			cs.registerOutParameter(3, java.sql.Types.INTEGER);
			cs.registerOutParameter(4, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			dataset.setResultSuccess(cs.getInt(3));
			dataset.setErrorMessage(cs.getString(4));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return dataset;
	}



	public List<Prediction> getPredictionByPatient(int patientID) {
		List<Prediction> listDataset = new ArrayList<Prediction>();
		Connection connection = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_PredictionHistoryGetByPatient(?)}");
			cs.setInt(1, patientID);
			rs = cs.executeQuery();
			while(rs.next()) {
				Prediction pred = new Prediction();
				pred.setPredictionID(rs.getInt("PredictionID"));
				pred.setDatasetID(rs.getInt("DatasetID"));
				pred.setFgGender(rs.getInt("FgGender"));
				pred.setFgGenderText(rs.getString("FgGenderText"));
				pred.setAge(rs.getInt("Age"));
				pred.setAgeText(rs.getString("AgeText"));
				pred.setFgActiveSmoker(rs.getInt("FgActiveSmoker"));
				pred.setFgActiveSmokerText(rs.getString("FgActiveSmokerText"));
				pred.setCigarettesPerDay(rs.getInt("CigarettesPerDay"));
				pred.setCigarettesPerDayText(rs.getString("CigarettesPerDayText"));
				pred.setFgStroke(rs.getInt("FgStroke"));
				pred.setFgStrokeText(rs.getString("FgStrokeText"));
				pred.setFgHighBlood(rs.getInt("FgHighBlood"));
				pred.setFgHighBloodText(rs.getString("FgHighBloodText"));
				pred.setFgDiabetes(rs.getInt("FgDiabetes"));
				pred.setFgDiabetesText(rs.getString("FgDiabetesText"));
				pred.setCholesterol(rs.getInt("Cholesterol"));
				pred.setCholesterolText(rs.getString("CholesterolText"));
				pred.setSistolikRate(rs.getFloat("SistolikRate"));
				pred.setSistolikRateText(rs.getString("SistolikRateText"));
				pred.setDiastolikRate(rs.getFloat("DiastolikRate"));
				pred.setDiastolikRateText(rs.getString("DiastolikRateText"));
				pred.setFgWeight(rs.getFloat("FgWeight"));
				pred.setFgWeightText(rs.getString("FgWeightText"));
				pred.setHeartRate(rs.getInt("HeartRate"));
				pred.setHeartRateText(rs.getString("HeartRateText"));
				pred.setFgVasoconstriction(rs.getInt("FgVasoconstriction"));
				pred.setFgVasoconstrictionText(rs.getString("FgVasoconstrictionText"));
				pred.setPredictionDate(rs.getDate("PredictionDate"));
				pred.setPredictionDateText(DateHelper.formatDateToString(rs.getDate("PredictionDate"), "dd-MM-YYYY"));
				listDataset.add(pred);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs, rs);
		}
		return listDataset;
	}
}