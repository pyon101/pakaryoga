package com.pakar.eao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import com.pakar.entity.Disease;
import com.pakar.utils.ReleaseConnection;
import com.pakar.utils.Settings;

@Stateless
public class DiseaseEao {
	
	public List<Disease> getDisease(int PatientID) {
    	List<Disease> listDisease = new ArrayList<Disease>();
    	Connection connection = null;
    	CallableStatement cs = null;
    	ResultSet rs = null;
    	try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_DiseaseGet (?)}");
			cs.setInt(1, PatientID);
			rs = cs.executeQuery();
			while(rs.next()) {
				Disease disease = new Disease();
				disease.setDiseaseID(rs.getInt("DiseaseID"));
				disease.setDiseaseName(rs.getString("DiseaseName"));
				disease.setDiseaseDesc(rs.getString("DiseaseDesc"));
				listDisease.add(disease);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs, rs);
		}
    	return listDisease;
    }
	
	public Disease diseaseAdd(int PatientID, Disease ds, String User) {
		System.out.println(PatientID + " | " + ds.getDiseaseName() + " | " + ds.getDiseaseDesc());
		Disease disease = new Disease();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_DiseaseAdd (?,?,?,?,?,?) }");
			cs.setInt(1, PatientID);
			cs.setString(2, ds.getDiseaseName());
			cs.setString(3, ds.getDiseaseDesc());
			cs.setString(4, User);
			cs.registerOutParameter(5, java.sql.Types.INTEGER);
			cs.registerOutParameter(6, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			disease.setResultSuccess(cs.getInt(5));
			disease.setErrorMessage(cs.getString(6));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return disease;
	}
	
	public Disease diseaseEdit(Disease ds, String User) {
		System.out.println(ds.getDiseaseID() + " | " + ds.getDiseaseName() + " | " + ds.getDiseaseDesc());
		Disease disease = new Disease();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_DiseaseEdit (?,?,?,?,?,?) }");
			cs.setInt(1, ds.getDiseaseID());
			cs.setString(2, ds.getDiseaseName());
			cs.setString(3, ds.getDiseaseDesc());
			cs.setString(4, User);
			cs.registerOutParameter(5, java.sql.Types.INTEGER);
			cs.registerOutParameter(6, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			disease.setResultSuccess(cs.getInt(5));
			disease.setErrorMessage(cs.getString(6));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return disease;
	}
	
	public Disease diseaseDelete(int DiseaseID, String User) {
		Disease disease = new Disease();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_DiseaseDelete (?,?,?,?) }");
			cs.setInt(1, DiseaseID);
			cs.setString(2, User);
			cs.registerOutParameter(3, java.sql.Types.INTEGER);
			cs.registerOutParameter(4, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			disease.setResultSuccess(cs.getInt(3));
			disease.setErrorMessage(cs.getString(4));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return disease;
	}
}