package com.pakar.eao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import com.pakar.entity.Dataset;
import com.pakar.utils.ReleaseConnection;
import com.pakar.utils.Settings;

@Stateless
public class DatasetEao {
	
	public List<Dataset> getDataset() {
    	List<Dataset> listDataset = new ArrayList<Dataset>();
    	Connection connection = null;
    	CallableStatement cs = null;
    	ResultSet rs = null;
    	try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_DatasetGet}");
			rs = cs.executeQuery();
			while(rs.next()) {
				Dataset dataset = new Dataset();
				dataset.setDatasetID(rs.getInt("DatasetID"));
				dataset.setFgGender(rs.getInt("FgGender"));
				dataset.setFgGenderText(rs.getString("FgGenderText"));
				dataset.setAge(rs.getInt("Age"));
				dataset.setAgeText(rs.getString("AgeText"));
				dataset.setFgActiveSmoker(rs.getInt("FgActiveSmoker"));
				dataset.setFgActiveSmokerText(rs.getString("FgActiveSmokerText"));
				dataset.setCigarettesPerDay(rs.getInt("CigarettesPerDay"));
				dataset.setCigarettesPerDayText(rs.getString("CigarettesPerDayText"));
				dataset.setFgStroke(rs.getInt("FgStroke"));
				dataset.setFgStrokeText(rs.getString("FgStrokeText"));
				dataset.setFgHighBlood(rs.getInt("FgHighBlood"));
				dataset.setFgHighBloodText(rs.getString("FgHighBloodText"));
				dataset.setFgDiabetes(rs.getInt("FgDiabetes"));
				dataset.setFgDiabetesText(rs.getString("FgDiabetesText"));
				dataset.setCholesterol(rs.getInt("Cholesterol"));
				dataset.setCholesterolText(rs.getString("CholesterolText"));
				dataset.setSistolikRate(rs.getFloat("SistolikRate"));
				dataset.setSistolikRateText(rs.getString("SistolikRateText"));
				dataset.setDiastolikRate(rs.getFloat("DiastolikRate"));
				dataset.setDiastolikRateText(rs.getString("DiastolikRateText"));
				dataset.setFgWeight(rs.getFloat("FgWeight"));
				dataset.setFgWeightText(rs.getString("FgWeightText"));
				dataset.setHeartRate(rs.getInt("HeartRate"));
				dataset.setHeartRateText(rs.getString("HeartRateText"));
				dataset.setFgVasoconstriction(rs.getInt("FgVasoconstriction"));
				dataset.setFgVasoconstrictionText(rs.getString("FgVasoconstrictionText"));
				listDataset.add(dataset);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs, rs);
		}
    	return listDataset;
    }
	
	public Dataset datasetAdd(Dataset ds, String User) {
		System.out.println(ds.getFgGender() + " | " + ds.getAge() + " | " + ds.getFgActiveSmoker() + " | " + ds.getCigarettesPerDay() + " | " + ds.getFgStroke() + " | " + ds.getFgHighBlood() + " | " + ds.getFgDiabetes());
		System.out.println(ds.getCholesterol() + " | " + ds.getSistolikRate() + " | " + ds.getDiastolikRate() + " | " + ds.getFgWeight() + " | " + ds.getHeartRate() + " | " + ds.getFgVasoconstriction() + " | " + User);
		Dataset dataset = new Dataset();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_DatasetAdd (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
			cs.setInt(1, ds.getFgGender());
			cs.setInt(2, ds.getAge());
			cs.setInt(3, ds.getFgActiveSmoker());
			cs.setInt(4, ds.getCigarettesPerDay());
			cs.setInt(5, ds.getFgStroke());
			cs.setInt(6, ds.getFgHighBlood());
			cs.setInt(7, ds.getFgDiabetes());
			cs.setInt(8, ds.getCholesterol());
			cs.setFloat(9, ds.getSistolikRate());
			cs.setFloat(10, ds.getDiastolikRate());
			cs.setFloat(11, ds.getFgWeight());
			cs.setInt(12, ds.getHeartRate());
			cs.setInt(13, ds.getFgVasoconstriction());
			cs.setString(14, User);
			cs.registerOutParameter(15, java.sql.Types.INTEGER);
			cs.registerOutParameter(16, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			dataset.setResultSuccess(cs.getInt(15));
			dataset.setErrorMessage(cs.getString(16));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return dataset;
	}
	
	public Dataset datasetEdit(Dataset ds, String User) {
		System.out.println(ds.getDatasetID() + " | " + ds.getFgGender() + " | " + ds.getAge() + " | " + ds.getFgActiveSmoker() + " | " + ds.getCigarettesPerDay() + " | " + ds.getFgStroke() + " | " + ds.getFgHighBlood() + " | " + ds.getFgDiabetes());
		System.out.println(ds.getCholesterol() + " | " + ds.getSistolikRate() + " | " + ds.getDiastolikRate() + " | " + ds.getFgWeight() + " | " + ds.getHeartRate() + " | " + ds.getFgVasoconstriction() + " | " + User);
		Dataset dataset = new Dataset();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_DatasetEdit (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
			cs.setInt(1, ds.getDatasetID());
			cs.setInt(2, ds.getFgGender());
			cs.setInt(3, ds.getAge());
			cs.setInt(4, ds.getFgActiveSmoker());
			cs.setInt(5, ds.getCigarettesPerDay());
			cs.setInt(6, ds.getFgStroke());
			cs.setInt(7, ds.getFgHighBlood());
			cs.setInt(8, ds.getFgDiabetes());
			cs.setInt(9, ds.getCholesterol());
			cs.setFloat(10, ds.getSistolikRate());
			cs.setFloat(11, ds.getDiastolikRate());
			cs.setFloat(12, ds.getFgWeight());
			cs.setInt(13, ds.getHeartRate());
			cs.setInt(14, ds.getFgVasoconstriction());
			cs.setString(15, User);
			cs.registerOutParameter(16, java.sql.Types.INTEGER);
			cs.registerOutParameter(17, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			dataset.setResultSuccess(cs.getInt(16));
			dataset.setErrorMessage(cs.getString(17));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return dataset;
	}
	
	public Dataset datasetDelete(int DatasetID, String User) {
		Dataset dataset = new Dataset();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_DatasetDelete (?,?,?,?) }");
			cs.setInt(1, DatasetID);
			cs.setString(2, User);
			cs.registerOutParameter(3, java.sql.Types.INTEGER);
			cs.registerOutParameter(4, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			dataset.setResultSuccess(cs.getInt(3));
			dataset.setErrorMessage(cs.getString(4));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return dataset;
	}
}