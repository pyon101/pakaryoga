package com.pakar.eao;

import java.sql.CallableStatement;
import java.sql.Connection;

import javax.ejb.Stateless;

import com.pakar.entity.User;
import com.pakar.utils.ReleaseConnection;
import com.pakar.utils.Settings;

@Stateless
public class UserEao {
	
	public User userRegister(User UserIn) {
		User user = new User();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_UserRegister (?,?,?,?,?,?) }");
			cs.setString(1, UserIn.getEmail());
			cs.setString(2, UserIn.getName());
			cs.setString(3, UserIn.getMobilePhone());
			cs.setString(4, UserIn.getConfirmPassword());
			cs.registerOutParameter(5, java.sql.Types.INTEGER);
			cs.registerOutParameter(6, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			user.setResultSuccess(cs.getInt(5));
			user.setErrorMessage(cs.getString(6));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return user;
	}
	
	public User userLogin(String Email, String Password) {
		User user = new User();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_UserLogin (?,?,?,?) }");
			cs.setString(1, Email);
			cs.setString(2, Password);
			cs.registerOutParameter(3, java.sql.Types.INTEGER);
			cs.registerOutParameter(4, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			user.setResultSuccess(cs.getInt(3));
			user.setErrorMessage(cs.getString(4));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return user;
	}
	
	public User userChangePassword(String Email, String Password) {
		User user = new User();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_UserChangePassword (?,?,?,?) }");
			cs.setString(1, Email);
			cs.setString(2, Password);
			cs.registerOutParameter(3, java.sql.Types.INTEGER);
			cs.registerOutParameter(4, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			user.setResultSuccess(cs.getInt(3));
			user.setErrorMessage(cs.getString(4));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return user;
	}
	
	public User getUserAllData(String Email) {
		User user = new User();
		Connection connection = null;
		CallableStatement cs = null;
		try {
			connection = Settings.getConnection();
			cs = connection.prepareCall("{call sp_UserGetAllData (?,?,?,?,?,?) }");
			cs.setString(1, Email);
			cs.registerOutParameter(2, java.sql.Types.VARCHAR);
			cs.registerOutParameter(3, java.sql.Types.VARCHAR);
			cs.registerOutParameter(4, java.sql.Types.VARCHAR);
			cs.registerOutParameter(5, java.sql.Types.INTEGER);
			cs.registerOutParameter(6, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			user.setName(cs.getString(2));
			user.setPhoneCode(cs.getString(3));
			user.setMobilePhone(cs.getString(4));
			user.setResultSuccess(cs.getInt(5));
			user.setErrorMessage(cs.getString(6));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReleaseConnection.close(connection, cs);
		}
		return user;
	}
}