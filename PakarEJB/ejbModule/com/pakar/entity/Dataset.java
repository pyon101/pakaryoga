package com.pakar.entity;

public class Dataset extends BasicEntity {
	
	private int datasetID;    
	private int fgGender;  
	private int age;
	private int fgActiveSmoker;
	private int cigarettesPerDay;
	private int fgStroke;
	private int fgHighBlood;
	private int fgDiabetes;
	private int cholesterol;
	private int heartRate;
	private int fgVasoconstriction;
	private int rowNumber;
	
	private float sistolikRate;
	private float diastolikRate;
	private float fgWeight;
	
	private String fgGenderText;  
	private String ageText;
	private String fgActiveSmokerText;
	private String cigarettesPerDayText;
	private String fgStrokeText;
	private String fgHighBloodText;
	private String fgDiabetesText;
	private String cholesterolText;
	private String sistolikRateText;
	private String diastolikRateText;
	private String fgWeightText;
	private String heartRateText;
	private String fgVasoconstrictionText;
	
	public Dataset() {}
	
	public Dataset(int datasetID, int fgGender, int age, int fgActiveSmoker, int cigarettesPerDay, int fgStroke,
			int fgHighBlood, int fgDiabetes, int cholesterol, int heartRate, int fgVasoconstriction, int rowNumber,
			float sistolikRate, float diastolikRate, float fgWeight, String fgGenderText, String ageText,
			String fgActiveSmokerText, String cigarettesPerDayText, String fgStrokeText, String fgHighBloodText,
			String fgDiabetesText, String cholesterolText, String sistolikRateText, String diastolikRateText,
			String fgWeightText, String heartRateText, String fgVasoconstrictionText) {
		super();
		this.datasetID = datasetID;
		this.fgGender = fgGender;
		this.age = age;
		this.fgActiveSmoker = fgActiveSmoker;
		this.cigarettesPerDay = cigarettesPerDay;
		this.fgStroke = fgStroke;
		this.fgHighBlood = fgHighBlood;
		this.fgDiabetes = fgDiabetes;
		this.cholesterol = cholesterol;
		this.heartRate = heartRate;
		this.fgVasoconstriction = fgVasoconstriction;
		this.rowNumber = rowNumber;
		this.sistolikRate = sistolikRate;
		this.diastolikRate = diastolikRate;
		this.fgWeight = fgWeight;
		this.fgGenderText = fgGenderText;
		this.ageText = ageText;
		this.fgActiveSmokerText = fgActiveSmokerText;
		this.cigarettesPerDayText = cigarettesPerDayText;
		this.fgStrokeText = fgStrokeText;
		this.fgHighBloodText = fgHighBloodText;
		this.fgDiabetesText = fgDiabetesText;
		this.cholesterolText = cholesterolText;
		this.sistolikRateText = sistolikRateText;
		this.diastolikRateText = diastolikRateText;
		this.fgWeightText = fgWeightText;
		this.heartRateText = heartRateText;
		this.fgVasoconstrictionText = fgVasoconstrictionText;
	}

	public int getRowNumber() {
		return rowNumber;
	}
	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}
	public int getDatasetID() {
		return datasetID;
	}
	public void setDatasetID(int datasetID) {
		this.datasetID = datasetID;
	}
	public int getFgGender() {
		return fgGender;
	}
	public void setFgGender(int fgGender) {
		this.fgGender = fgGender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getFgActiveSmoker() {
		return fgActiveSmoker;
	}
	public void setFgActiveSmoker(int fgActiveSmoker) {
		this.fgActiveSmoker = fgActiveSmoker;
	}
	public int getCigarettesPerDay() {
		return cigarettesPerDay;
	}
	public void setCigarettesPerDay(int cigarettesPerDay) {
		this.cigarettesPerDay = cigarettesPerDay;
	}
	public int getFgStroke() {
		return fgStroke;
	}
	public void setFgStroke(int fgStroke) {
		this.fgStroke = fgStroke;
	}
	public int getFgHighBlood() {
		return fgHighBlood;
	}
	public void setFgHighBlood(int fgHighBlood) {
		this.fgHighBlood = fgHighBlood;
	}
	public int getFgDiabetes() {
		return fgDiabetes;
	}
	public void setFgDiabetes(int fgDiabetes) {
		this.fgDiabetes = fgDiabetes;
	}
	public int getCholesterol() {
		return cholesterol;
	}
	public void setCholesterol(int cholesterol) {
		this.cholesterol = cholesterol;
	}
	public int getHeartRate() {
		return heartRate;
	}
	public void setHeartRate(int heartRate) {
		this.heartRate = heartRate;
	}
	public int getFgVasoconstriction() {
		return fgVasoconstriction;
	}
	public void setFgVasoconstriction(int fgVasoconstriction) {
		this.fgVasoconstriction = fgVasoconstriction;
	}
	public float getSistolikRate() {
		return sistolikRate;
	}
	public void setSistolikRate(float sistolikRate) {
		this.sistolikRate = sistolikRate;
	}
	public float getDiastolikRate() {
		return diastolikRate;
	}
	public void setDiastolikRate(float diastolikRate) {
		this.diastolikRate = diastolikRate;
	}
	public float getFgWeight() {
		return fgWeight;
	}
	public void setFgWeight(float fgWeight) {
		this.fgWeight = fgWeight;
	}
	public String getFgGenderText() {
		return fgGenderText;
	}
	public void setFgGenderText(String fgGenderText) {
		this.fgGenderText = fgGenderText;
	}
	public String getAgeText() {
		return ageText;
	}
	public void setAgeText(String ageText) {
		this.ageText = ageText;
	}
	public String getFgActiveSmokerText() {
		return fgActiveSmokerText;
	}
	public void setFgActiveSmokerText(String fgActiveSmokerText) {
		this.fgActiveSmokerText = fgActiveSmokerText;
	}
	public String getCigarettesPerDayText() {
		return cigarettesPerDayText;
	}
	public void setCigarettesPerDayText(String cigarettesPerDayText) {
		this.cigarettesPerDayText = cigarettesPerDayText;
	}
	public String getFgStrokeText() {
		return fgStrokeText;
	}
	public void setFgStrokeText(String fgStrokeText) {
		this.fgStrokeText = fgStrokeText;
	}
	public String getFgHighBloodText() {
		return fgHighBloodText;
	}
	public void setFgHighBloodText(String fgHighBloodText) {
		this.fgHighBloodText = fgHighBloodText;
	}
	public String getFgDiabetesText() {
		return fgDiabetesText;
	}
	public void setFgDiabetesText(String fgDiabetesText) {
		this.fgDiabetesText = fgDiabetesText;
	}
	public String getCholesterolText() {
		return cholesterolText;
	}
	public void setCholesterolText(String cholesterolText) {
		this.cholesterolText = cholesterolText;
	}
	public String getSistolikRateText() {
		return sistolikRateText;
	}
	public void setSistolikRateText(String sistolikRateText) {
		this.sistolikRateText = sistolikRateText;
	}
	public String getDiastolikRateText() {
		return diastolikRateText;
	}
	public void setDiastolikRateText(String diastolikRateText) {
		this.diastolikRateText = diastolikRateText;
	}
	public String getFgWeightText() {
		return fgWeightText;
	}
	public void setFgWeightText(String fgWeightText) {
		this.fgWeightText = fgWeightText;
	}
	public String getHeartRateText() {
		return heartRateText;
	}
	public void setHeartRateText(String heartRateText) {
		this.heartRateText = heartRateText;
	}
	public String getFgVasoconstrictionText() {
		return fgVasoconstrictionText;
	}
	public void setFgVasoconstrictionText(String fgVasoconstrictionText) {
		this.fgVasoconstrictionText = fgVasoconstrictionText;
	}
}