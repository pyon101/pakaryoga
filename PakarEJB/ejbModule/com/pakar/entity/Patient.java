package com.pakar.entity;

import java.util.Date;

public class Patient extends BasicEntity {
	
	private int patientID;
	private int fgGender;
	private int patientAge;
	private int patientWeight;
	private int patientHeight;
	
	private float patientHeightMeter;
	private float patientWeightBMI;
	
	private String fgGenderText;
	private String patientName;
	private String birthOfDateView;
	private String phoneCode;
	private String mobilePhone;
	private String address;
	
	private Date birthOfDate;

	public Patient() {}

	public Patient(int patientID, int fgGender, int patientAge, int patientWeight, int patientHeight,
			float patientHeightMeter, float patientWeightBMI, String fgGenderText, String patientName,
			String birthOfDateView, String phoneCode, String mobilePhone, String address, Date birthOfDate) {
		super();
		this.patientID = patientID;
		this.fgGender = fgGender;
		this.patientAge = patientAge;
		this.patientWeight = patientWeight;
		this.patientHeight = patientHeight;
		this.patientHeightMeter = patientHeightMeter;
		this.patientWeightBMI = patientWeightBMI;
		this.fgGenderText = fgGenderText;
		this.patientName = patientName;
		this.birthOfDateView = birthOfDateView;
		this.phoneCode = phoneCode;
		this.mobilePhone = mobilePhone;
		this.address = address;
		this.birthOfDate = birthOfDate;
	}

	public float getPatientHeightMeter() {
		return patientHeightMeter;
	}

	public void setPatientHeightMeter(float patientHeightMeter) {
		this.patientHeightMeter = patientHeightMeter;
	}

	public int getPatientWeight() {
		return patientWeight;
	}

	public void setPatientWeight(int patientWeight) {
		this.patientWeight = patientWeight;
	}

	public int getPatientHeight() {
		return patientHeight;
	}

	public void setPatientHeight(int patientHeight) {
		this.patientHeight = patientHeight;
	}

	public float getPatientWeightBMI() {
		return patientWeightBMI;
	}

	public void setPatientWeightBMI(float patientWeightBMI) {
		this.patientWeightBMI = patientWeightBMI;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	public int getPatientAge() {
		return patientAge;
	}

	public void setPatientAge(int patientAge) {
		this.patientAge = patientAge;
	}

	public int getPatientID() {
		return patientID;
	}

	public void setPatientID(int patientID) {
		this.patientID = patientID;
	}

	public int getFgGender() {
		return fgGender;
	}

	public void setFgGender(int fgGender) {
		this.fgGender = fgGender;
	}

	public String getFgGenderText() {
		return fgGenderText;
	}

	public void setFgGenderText(String fgGenderText) {
		this.fgGenderText = fgGenderText;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getBirthOfDateView() {
		return birthOfDateView;
	}

	public void setBirthOfDateView(String birthOfDateView) {
		this.birthOfDateView = birthOfDateView;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirthOfDate() {
		return birthOfDate;
	}

	public void setBirthOfDate(Date birthOfDate) {
		this.birthOfDate = birthOfDate;
	}
}