package com.pakar.entity;

import java.sql.Date;

public class Prediction extends BasicEntity {
	
	private int predictionID;
	private int datasetID;    
	private int fgGender;  
	private int age;
	private int fgActiveSmoker;
	private int cigarettesPerDay;
	private int fgStroke;
	private int fgHighBlood;
	private int fgDiabetes;
	private int cholesterol;
	private int heartRate;
	private int fgVasoconstriction;
	private int rowNumber;
	private int predictionDataset;
	private int predictionNB;
	private int predictionFgVasoconstriction;
	
	private float sistolikRate;
	private float diastolikRate;
	private float fgWeight;
	
	private Double predictionSuccess;
	private Double predictionFailed;
	private Double predictionResult;
	
	private String predictionDateText;
	private String resultText;
	private String fgGenderText;  
	private String ageText;
	private String fgActiveSmokerText;
	private String cigarettesPerDayText;
	private String fgStrokeText;
	private String fgHighBloodText;
	private String fgDiabetesText;
	private String cholesterolText;
	private String sistolikRateText;
	private String diastolikRateText;
	private String fgWeightText;
	private String heartRateText;
	private String fgVasoconstrictionText;
	
	private Date predictionDate;
	
	
	public int getPredictionDataset() {
		return predictionDataset;
	}

	public void setPredictionDataset(int predictionDataset) {
		this.predictionDataset = predictionDataset;
	}

	public int getPredictionNB() {
		return predictionNB;
	}

	public void setPredictionNB(int predictionNB) {
		this.predictionNB = predictionNB;
	}

	public String getPredictionDateText() {
		return predictionDateText;
	}

	public void setPredictionDateText(String predictionDateText) {
		this.predictionDateText = predictionDateText;
	}

	public Date getPredictionDate() {
		return predictionDate;
	}

	public void setPredictionDate(Date predictionDate) {
		this.predictionDate = predictionDate;
	}

	public String getResultText() {
		return resultText;
	}

	public void setResultText(String resultText) {
		this.resultText = resultText;
	}

	public int getPredictionID() {
		return predictionID;
	}

	public void setPredictionID(int predictionID) {
		this.predictionID = predictionID;
	}

	public int getDatasetID() {
		return datasetID;
	}

	public void setDatasetID(int datasetID) {
		this.datasetID = datasetID;
	}

	public int getFgGender() {
		return fgGender;
	}

	public void setFgGender(int fgGender) {
		this.fgGender = fgGender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getFgActiveSmoker() {
		return fgActiveSmoker;
	}

	public void setFgActiveSmoker(int fgActiveSmoker) {
		this.fgActiveSmoker = fgActiveSmoker;
	}

	public int getCigarettesPerDay() {
		return cigarettesPerDay;
	}

	public void setCigarettesPerDay(int cigarettesPerDay) {
		this.cigarettesPerDay = cigarettesPerDay;
	}

	public int getFgStroke() {
		return fgStroke;
	}

	public void setFgStroke(int fgStroke) {
		this.fgStroke = fgStroke;
	}

	public int getFgHighBlood() {
		return fgHighBlood;
	}

	public void setFgHighBlood(int fgHighBlood) {
		this.fgHighBlood = fgHighBlood;
	}

	public int getFgDiabetes() {
		return fgDiabetes;
	}

	public void setFgDiabetes(int fgDiabetes) {
		this.fgDiabetes = fgDiabetes;
	}

	public int getCholesterol() {
		return cholesterol;
	}

	public void setCholesterol(int cholesterol) {
		this.cholesterol = cholesterol;
	}

	public int getHeartRate() {
		return heartRate;
	}

	public void setHeartRate(int heartRate) {
		this.heartRate = heartRate;
	}

	public int getFgVasoconstriction() {
		return fgVasoconstriction;
	}

	public void setFgVasoconstriction(int fgVasoconstriction) {
		this.fgVasoconstriction = fgVasoconstriction;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public float getSistolikRate() {
		return sistolikRate;
	}

	public void setSistolikRate(float sistolikRate) {
		this.sistolikRate = sistolikRate;
	}

	public float getDiastolikRate() {
		return diastolikRate;
	}

	public void setDiastolikRate(float diastolikRate) {
		this.diastolikRate = diastolikRate;
	}

	public float getFgWeight() {
		return fgWeight;
	}

	public void setFgWeight(float fgWeight) {
		this.fgWeight = fgWeight;
	}

	public String getFgGenderText() {
		return fgGenderText;
	}

	public void setFgGenderText(String fgGenderText) {
		this.fgGenderText = fgGenderText;
	}

	public String getAgeText() {
		return ageText;
	}

	public void setAgeText(String ageText) {
		this.ageText = ageText;
	}

	public String getFgActiveSmokerText() {
		return fgActiveSmokerText;
	}

	public void setFgActiveSmokerText(String fgActiveSmokerText) {
		this.fgActiveSmokerText = fgActiveSmokerText;
	}

	public String getCigarettesPerDayText() {
		return cigarettesPerDayText;
	}

	public void setCigarettesPerDayText(String cigarettesPerDayText) {
		this.cigarettesPerDayText = cigarettesPerDayText;
	}

	public String getFgStrokeText() {
		return fgStrokeText;
	}

	public void setFgStrokeText(String fgStrokeText) {
		this.fgStrokeText = fgStrokeText;
	}

	public String getFgHighBloodText() {
		return fgHighBloodText;
	}

	public void setFgHighBloodText(String fgHighBloodText) {
		this.fgHighBloodText = fgHighBloodText;
	}

	public String getFgDiabetesText() {
		return fgDiabetesText;
	}

	public void setFgDiabetesText(String fgDiabetesText) {
		this.fgDiabetesText = fgDiabetesText;
	}

	public String getCholesterolText() {
		return cholesterolText;
	}

	public void setCholesterolText(String cholesterolText) {
		this.cholesterolText = cholesterolText;
	}

	public String getSistolikRateText() {
		return sistolikRateText;
	}

	public void setSistolikRateText(String sistolikRateText) {
		this.sistolikRateText = sistolikRateText;
	}

	public String getDiastolikRateText() {
		return diastolikRateText;
	}

	public void setDiastolikRateText(String diastolikRateText) {
		this.diastolikRateText = diastolikRateText;
	}

	public String getFgWeightText() {
		return fgWeightText;
	}

	public void setFgWeightText(String fgWeightText) {
		this.fgWeightText = fgWeightText;
	}

	public String getHeartRateText() {
		return heartRateText;
	}

	public void setHeartRateText(String heartRateText) {
		this.heartRateText = heartRateText;
	}

	public String getFgVasoconstrictionText() {
		return fgVasoconstrictionText;
	}

	public void setFgVasoconstrictionText(String fgVasoconstrictionText) {
		this.fgVasoconstrictionText = fgVasoconstrictionText;
	}

	public Double getPredictionSuccess() {
		return predictionSuccess;
	}

	public void setPredictionSuccess(Double predictionSuccess) {
		this.predictionSuccess = predictionSuccess;
	}

	public Double getPredictionFailed() {
		return predictionFailed;
	}

	public void setPredictionFailed(Double predictionFailed) {
		this.predictionFailed = predictionFailed;
	}

	public Double getPredictionResult() {
		return predictionResult;
	}

	public void setPredictionResult(Double predictionResult) {
		this.predictionResult = predictionResult;
	}

	public int getPredictionFgVasoconstriction() {
		return predictionFgVasoconstriction;
	}

	public void setPredictionFgVasoconstriction(int predictionFgVasoconstriction) {
		this.predictionFgVasoconstriction = predictionFgVasoconstriction;
	}
}