package com.pakar.entity;

public class Report extends BasicEntity {
	private String name          ;
	private String address       ;
	private String phone         ;
	private String gender        ;
	private String age           ;
	private String birth         ;
	private String bmi           ;
	private String prediction    ;
	private String warts         ;
	private String stroke        ;
	private String blood         ;
	private String kolesterol    ;
	private String sistolik      ;
	private String diastolik     ;
	private String heartBeat     ;
	private String result        ;
	private String diabetes      ;
	private String smoker        ;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getBmi() {
		return bmi;
	}
	public void setBmi(String bmi) {
		this.bmi = bmi;
	}
	public String getPrediction() {
		return prediction;
	}
	public void setPrediction(String prediction) {
		this.prediction = prediction;
	}
	public String getWarts() {
		return warts;
	}
	public void setWarts(String warts) {
		this.warts = warts;
	}
	public String getStroke() {
		return stroke;
	}
	public void setStroke(String stroke) {
		this.stroke = stroke;
	}
	public String getBlood() {
		return blood;
	}
	public void setBlood(String blood) {
		this.blood = blood;
	}
	public String getKolesterol() {
		return kolesterol;
	}
	public void setKolesterol(String kolesterol) {
		this.kolesterol = kolesterol;
	}
	public String getSistolik() {
		return sistolik;
	}
	public void setSistolik(String sistolik) {
		this.sistolik = sistolik;
	}
	public String getDiastolik() {
		return diastolik;
	}
	public void setDiastolik(String diastolik) {
		this.diastolik = diastolik;
	}
	public String getHeartBeat() {
		return heartBeat;
	}
	public void setHeartBeat(String heartBeat) {
		this.heartBeat = heartBeat;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getDiabetes() {
		return diabetes;
	}
	public void setDiabetes(String diabetes) {
		this.diabetes = diabetes;
	}
	public String getSmoker() {
		return smoker;
	}
	public void setSmoker(String smoker) {
		this.smoker = smoker;
	}
	
	
}