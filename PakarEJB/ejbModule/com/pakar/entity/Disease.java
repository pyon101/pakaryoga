package com.pakar.entity;

public class Disease extends BasicEntity {
	
	private int diseaseID;
	
	private String diseaseName;
	private String diseaseDesc;
	
	public Disease() {}
	
	public Disease(int diseaseID, String diseaseName, String diseaseDesc) {
		super();
		this.diseaseID = diseaseID;
		this.diseaseName = diseaseName;
		this.diseaseDesc = diseaseDesc;
	}

	public int getDiseaseID() {
		return diseaseID;
	}

	public void setDiseaseID(int diseaseID) {
		this.diseaseID = diseaseID;
	}

	public String getDiseaseName() {
		return diseaseName;
	}

	public void setDiseaseName(String diseaseName) {
		this.diseaseName = diseaseName;
	}

	public String getDiseaseDesc() {
		return diseaseDesc;
	}

	public void setDiseaseDesc(String diseaseDesc) {
		this.diseaseDesc = diseaseDesc;
	}
}