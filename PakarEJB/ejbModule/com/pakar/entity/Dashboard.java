package com.pakar.entity;

public class Dashboard extends BasicEntity {
	
	private int totalDataset;
	private int totalPrediction;
	private int totalPatient;
	
	public int getTotalDataset() {
		return totalDataset;
	}
	public void setTotalDataset(int totalDataset) {
		this.totalDataset = totalDataset;
	}
	public int getTotalPrediction() {
		return totalPrediction;
	}
	public void setTotalPrediction(int totalPrediction) {
		this.totalPrediction = totalPrediction;
	}
	public int getTotalPatient() {
		return totalPatient;
	}
	public void setTotalPatient(int totalPatient) {
		this.totalPatient = totalPatient;
	}
}