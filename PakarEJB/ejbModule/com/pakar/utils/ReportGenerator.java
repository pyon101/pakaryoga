package com.pakar.utils;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter; 
import net.sf.jasperreports.engine.JRParameter; 
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager; 
import net.sf.jasperreports.engine.JasperFillManager; 
import net.sf.jasperreports.engine.JasperPrint; 
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JExcelApiExporter;  
import net.sf.jasperreports.engine.export.JRCsvExporter;  
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporterParameter;
import net.sf.jasperreports.view.JasperViewer;
import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.ColumnBuilderException;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;

/**
 * ReportGenerator.java
 * To generate report in PDF, CSV and Excel format
 * 
 * */
public class ReportGenerator {
	
	public ReportGenerator() {}
	
	/** 
	  * Prints graphics report in PDF format. 
	  * 
	  * @param params the params
	  * @param reportFileName the reportfilename 
	  * @param resultSet resultSet for the report data 
	  * 
	  * @return true, if prints the report 
	  */  
	public final boolean exportGraphicReportToPDF(HashMap<String, Object> params, String reportFileName, String reportName , JRBeanCollectionDataSource beanColDataSource) {   			
		return printGraphicReport("P", params, reportFileName,reportName , beanColDataSource);  
	}  
	
	public final boolean exportGraphicReportToPDFdl(HashMap<String, Object> params, String reportFileName, String reportName , JRBeanCollectionDataSource beanColDataSource) {   			
		return printGraphicReport("PE", params, reportFileName,reportName , beanColDataSource);  
	}  
	
	/** 
	  * Prints graphics report in CSV format. 
	  * 
	  * @param params the params
	  * @param reportFileName the reportfilename 
	  * @param resultSet resultSet for the report data 
	  * 
	  * @return true, if prints the report 
	  */  
	
	public final boolean exportGraphicReportToCSV(HashMap<String, Object> params, String reportFileName,String reportName, JRBeanCollectionDataSource beanColDataSource) {   			
		return printGraphicReport("C", params, reportFileName,reportName, beanColDataSource);  
	}  
	
	/** 
	  * Prints graphics report in Excel format. 
	  * 
	  * @param params the params
	  * @param reportFileName the reportfilename 
	  * @param resultSet resultSet for the report data 
	  * 
	  * @return true, if prints the report 
	  */  	
	public final boolean exportGraphicReportToExcel(HashMap<String, Object> params, String reportFileName, String reportName,  JRBeanCollectionDataSource beanColDataSource) {   			
		return printGraphicReport("E", params, reportFileName,reportName , beanColDataSource);  
	}  
	
	/** 
	  * Prints report in PDF format. 
	  * 
	  * @param params the params
	  * @param reportFileName the reportfilename 
	  * @param resultSet resultSet for the report data 
	  * 
	  * @return true, if prints the report 
	  */  
	public final boolean exportReportToPDF(HashMap<String, Object> params, String reportFileName, ResultSet resultSet) {   			
		return printReport("P", params, reportFileName, resultSet);  
	}  

	/** 
	  * Prints report in CSV format. 
	  * 
	  * @param params the params
	  * @param reportFileName the reportfilename 
	  * @param resultSet resultSet for the report data 
	  * 
	  * @return true, if prints the report 
	  */  
	
	public final boolean exportReportToCSV(HashMap<String, Object> params, String reportFileName, ResultSet resultSet) {   			
		return printReport("C", params, reportFileName, resultSet);  
	}  

	/** 
	  * Prints report in Excel format. 
	  * 
	  * @param params the params
	  * @param reportFileName the reportfilename 
	  * @param resultSet resultSet for the report data 
	  * 
	  * @return true, if prints the report 
	  */  	
	public final boolean exportReportToExcel(HashMap<String, Object> params, String reportFileName, ResultSet resultSet) {   			
		return printReport("E", params, reportFileName, resultSet);  
	}  
	
	/** 
	 * @param format the format 
	 * @param params the params
	 * @param reportFileName the reportfilename 
	 * @param resultSet resultSet for the report data 
	 * 
	 * @return true, if prints the report 
	 */
	public final boolean printReport(String format, HashMap<String, Object> params, String reportFileName, ResultSet resultSet) { 
		try {			
			FacesContext context = FacesContext.getCurrentInstance();  
			JRResultSetDataSource resultDS = new JRResultSetDataSource(resultSet);			
			//HashMap<String, Object> parameters = new HashMap<String, Object>();			
			String jasperReportFileName = "/report/" + reportFileName + ".jasper";
			
			String reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath(jasperReportFileName);
			
			if ("P".equals(format)) {  
				params.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.FALSE);  
				
				JasperPrint jasperPrint = JasperFillManager.fillReport(reportPath, params, resultDS);
				HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
				response.setContentType("application/pdf");

				String filename = new StringBuffer(reportFileName).append(".pdf").toString();
				
				/**
				 * change from inline to attachment
				 * inline = the browser will try open the file in the browser with assosiate file
				 * attachment = the browser will ask to download the file
				 * */
				//response.addHeader("Content-Disposition", "inline;filename="  + filename);
				response.addHeader("Content-Disposition", "attachment;filename="  + filename);
				ServletOutputStream output = response.getOutputStream();			
				JasperExportManager.exportReportToPdfStream(jasperPrint, output);
				
			} else if ("C".equals(format)) { 
				params.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);  
				
				JasperPrint jasperPrint = JasperFillManager.fillReport(reportPath, params, resultDS);
				HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
				response.setContentType("text/plain");
				
				String filename = new StringBuffer(reportFileName).append(".csv").toString();

				response.addHeader("Content-Disposition", "attachment;filename="  + filename);
				
				JRCsvExporter exporter = new JRCsvExporter();  
				exporter.setParameter(JRCsvExporterParameter.JASPER_PRINT, jasperPrint);  
				exporter.setParameter(JRCsvExporterParameter.OUTPUT_STREAM, response.getOutputStream());  
				exporter.setParameter(JRExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);  
				exporter.exportReport(); 
				
			} else if ("E".equals(format)) {
				params.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
				
				JasperPrint jasperPrint = JasperFillManager.fillReport(reportPath, params, resultDS);
				HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
				response.setContentType("application/vnd.ms-excel");
				
				String filename = new StringBuffer(reportFileName).append(".xls").toString();

				response.addHeader("Content-Disposition", "attachment;filename="  + filename);
				
				JExcelApiExporter exporterXLS = new JExcelApiExporter();  
				exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);  
				exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);  
				exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);  
				exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);  
				exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);  
				exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response.getOutputStream());  
				exporterXLS.exportReport();  
			}
			context.getApplication().getStateManager().saveView(context);  
			context.responseComplete();  
			 
			return true;  
			
		}  catch (Exception e) {  
				System.out.println(e.getMessage());
		} finally {  
		}  
		
		return false;
	}
	
	
	/** 
	 * @param format the format 
	 * @param params the params
	 * @param reportFileName the reportfilename 
	 * @param JRBeanCollectionDataSource beanColDataSource for the report data 
	 * 
	 * @return true, if prints the report 
	 */
	public final boolean printGraphicReport(String format, HashMap<String, Object> params, String reportFileName,String reportName, JRBeanCollectionDataSource beanColDataSource) { 
		String filename;
		try {			
			FacesContext context = FacesContext.getCurrentInstance();  
			
			//HashMap<String, Object> parameters = new HashMap<String, Object>();			
			String jasperReportFileName = "/report/" + reportFileName + ".jasper";
			
			String reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath(jasperReportFileName);
			
			if ("P".equals(format)) {  
				params.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.FALSE);  
				
				JasperPrint jasperPrint = JasperFillManager.fillReport(reportPath, params, beanColDataSource);
				HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
				response.setContentType("application/pdf");
				if(reportName.equals(""))
					filename = new StringBuffer(reportFileName).append(".pdf").toString();
				else
					filename = new StringBuffer(reportName).append(".pdf").toString();
				/**
				 * change from inline to attachment
				 * inline = the browser will try open the file in the browser with assosiate file
				 * attachment = the browser will ask to download the file
				 * */
				response.addHeader("Content-Disposition", "inline;filename="  + filename);
				//response.addHeader("Content-Disposition", "attachment;filename="  + filename);
				ServletOutputStream output = response.getOutputStream();			
				JasperExportManager.exportReportToPdfStream(jasperPrint, output);
				//JasperViewer.viewReport(jasperPrint);
				
			} else if ("PE".equals(format)) {  
				params.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.FALSE);  
				
				JasperPrint jasperPrint = JasperFillManager.fillReport(reportPath, params, beanColDataSource);
				HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
				response.setContentType("application/pdf");
				if(reportName.equals(""))
					filename = new StringBuffer(reportFileName).append(".pdf").toString();
				else
					filename = new StringBuffer(reportName).append(".pdf").toString();
				/**
				 * change from inline to attachment
				 * inline = the browser will try open the file in the browser with assosiate file
				 * attachment = the browser will ask to download the file
				 * */
				//response.addHeader("Content-Disposition", "inline;filename="  + filename);
				response.addHeader("Content-Disposition", "attachment;filename="  + filename);
				ServletOutputStream output = response.getOutputStream();			
				JasperExportManager.exportReportToPdfStream(jasperPrint, output);
				//JasperViewer.viewReport(jasperPrint);
				
			}else if ("C".equals(format)) { 
				params.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);  
				
				JasperPrint jasperPrint = JasperFillManager.fillReport(reportPath, params, beanColDataSource);
				HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
				response.setContentType("text/plain");
				if(reportName.equals(""))
					 filename = new StringBuffer(reportFileName).append(".csv").toString();
				else
					filename = new StringBuffer(reportName).append(".csv").toString();

				response.addHeader("Content-Disposition", "attachment;filename="  + filename);
				
				JRCsvExporter exporter = new JRCsvExporter();  
				exporter.setParameter(JRCsvExporterParameter.JASPER_PRINT, jasperPrint);  
				exporter.setParameter(JRCsvExporterParameter.OUTPUT_STREAM, response.getOutputStream());  
				exporter.setParameter(JRExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);  
				exporter.exportReport(); 
				
				
			} else if ("E".equals(format)) {
				params.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);  
				
				JasperPrint jasperPrint = JasperFillManager.fillReport(reportPath, params, beanColDataSource);
				HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
				response.setContentType("application/vnd.ms-excel");
				if(reportName.equals(""))
					filename = new StringBuffer(reportFileName).append(".xls").toString();
				else
					filename = new StringBuffer(reportName).append(".xls").toString();

				response.addHeader("Content-Disposition", "attachment;filename="  + filename);
				
				JExcelApiExporter exporterXLS = new JExcelApiExporter();  
				exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);  
				exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);  
				exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);  
				exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);  
				exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);  
				exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response.getOutputStream());  
				exporterXLS.exportReport();  
				
				
				
			}
			context.getApplication().getStateManager().saveView(context);  
			context.responseComplete();  
			 
			return true;  
			
		}  catch (Exception e) {   
				System.out.println(e.getMessage());
		} finally {  
		}  
		
		return false;
	}
	
	
	
	
	public final boolean generateReportBankTranfer(String format , String title, String fileName, ResultSet rs, Connection conn, CallableStatement cs, String separator) {
		 if(format.equals("CSV")) {
			 System.out.println("format " + format);
			 return printReportDynamicCsv(title, fileName, rs, conn , cs,separator);
			 
		 }else if(format.equals("XLS")) {
			 System.out.println("format  "+ format);
			 return printReportDynamic(title, fileName, rs, conn , cs);
		 }else if(format.equals("TXT")){
			 System.out.println("format  " + format);
			 return printReportDynamicTxt(title, fileName, rs, conn , cs);
		 }else {
			 System.out.println("panjang format  " + format.length());
			 System.out.println("pformat  " + format);
			return false;
		 }
	}
	
	public final boolean generateReportDynamic(String title, String fileName, ResultSet query,Connection con , CallableStatement cs){
		
		return printReportDynamic(title, fileName, query, con , cs);
	}
	
	public final boolean generateReportCSV(String title, String fileName, ResultSet query,Connection con , CallableStatement cs , String separator){
		
		return printReportDynamicCsv(title, fileName, query, con , cs,separator);
	}
	
	public final boolean generateReportTxt(String title, String fileName, ResultSet query,Connection con , CallableStatement cs ){
		
		return printReportDynamicTxt(title, fileName, query, con , cs);
	}
	
	private boolean printReportDynamic(String title, String fileName, ResultSet query , Connection con , CallableStatement cs) {
		// TODO Auto-generated method stub
		String [] header = new String[100];
		boolean isPortrait=true;
	    int PORTRAIT_PAGE_HEIGHT = 842;
	    int PORTRAIT_PAGE_WIDTH = 595;
		try{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			
			ResultSetMetaData rsmd = query.getMetaData();
			int counts = rsmd.getColumnCount();
			for(int x=1; x<=counts; x++){
		        header[x] = rsmd.getColumnName(x);
		    }
			
			FastReportBuilder drb = new FastReportBuilder();
			
			
			for (int i = 1; i <= counts; i++){
				int columnWidth = PORTRAIT_PAGE_WIDTH / counts;
				String key = header[i];
				AbstractColumn column = null;
		        try {
		            column = ColumnBuilder.getNew().setColumnProperty(key, String.class.getName())
				            .setTitle(key).build();
		        } catch (ColumnBuilderException ex) {
		            ex.printStackTrace();
		        }
		          drb.addColumn(column); 
			}
			
			drb.setIgnorePagination(true);
			
			DynamicReport dynamicReport = drb.build();
			JRResultSetDataSource rsds = new JRResultSetDataSource(query);
			JasperPrint jasperPrint = DynamicJasperHelper.generateJasperPrint(dynamicReport, new ClassicLayoutManager(), rsds);
				
			
			HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
			response.setContentType("application/vnd.ms-excel");
			
			String filename = new StringBuffer(fileName).append(".xls").toString();
			response.addHeader("Content-Disposition", "attachment;filename="  + filename);
			
			/*ServletOutputStream output = response.getOutputStream();			
			JasperExportManager.exportReportToPdfStream(jasperPrint, output);*/
			String[] sheet = {"Sheet1"};
			JExcelApiExporter exporterXLS = new JExcelApiExporter();  
			exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);  
			exporterXLS.setParameter(JRXlsExporterParameter.SHEET_NAMES, sheet);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);  
			exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);  
			exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);  
			exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);  
			exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response.getOutputStream());  
			exporterXLS.exportReport();  
			
			//facesContext.getApplication().getStateManager().saveView(facesContext);  
			facesContext.responseComplete();
			
			
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			ReleaseConnection.close(con, cs, query);
		}
		return false;
	}
	
	private boolean printReportDynamicCsv(String title, String fileName, ResultSet query , Connection con , CallableStatement cs , String sparator ) {
		// TODO Auto-generated method stub
		String [] header = new String[100];
		boolean isPortrait=true;
	    int PORTRAIT_PAGE_HEIGHT = 842;
	    int PORTRAIT_PAGE_WIDTH = 595;
		try{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			
			ResultSetMetaData rsmd = query.getMetaData();
			int counts = rsmd.getColumnCount();
			for(int x=1; x<=counts; x++){
		        header[x] = rsmd.getColumnName(x);
		    }
			
			FastReportBuilder drb = new FastReportBuilder();
			
			
			for (int i = 1; i <= counts; i++){
				int columnWidth = PORTRAIT_PAGE_WIDTH / counts;
				String key = header[i];
				AbstractColumn column = null;
		        try {
		            column = ColumnBuilder.getNew().setColumnProperty(key, String.class.getName())
				            .setTitle(key).build();
		        } catch (ColumnBuilderException ex) {
		            ex.printStackTrace();
		        }
		          drb.addColumn(column); 
			}
			
			drb.setIgnorePagination(true);
			
			DynamicReport dynamicReport = drb.build();
			JRResultSetDataSource rsds = new JRResultSetDataSource(query);
			JasperPrint jasperPrint = DynamicJasperHelper.generateJasperPrint(dynamicReport, new ClassicLayoutManager(), rsds);
				
			
			HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
			response.setContentType("text/csv");
			
			String filename = new StringBuffer(fileName).append(".csv").toString();
			response.addHeader("Content-Disposition", "attachment;filename="  + filename);
			
			ServletOutputStream output = response.getOutputStream();			
			//JasperExportManager.exportReportToPdfStream(jasperPrint, output);
			
			JRCsvExporter exporterCSV = new JRCsvExporter();  
			exporterCSV.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);  
			exporterCSV.setParameter(JRExporterParameter.OUTPUT_STREAM, output);
			exporterCSV.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, sparator  );
			exporterCSV.exportReport();  
			
			//facesContext.getApplication().getStateManager().saveView(facesContext);  
			facesContext.responseComplete();
			
			
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			ReleaseConnection.close(con, cs, query);
		}
		return false;
	}
	
	private boolean printReportDynamicTxt(String title, String fileName, ResultSet query , Connection con , CallableStatement cs ) {
		// TODO Auto-generated method stub
		String [] header = new String[100];
		boolean isPortrait=true;
	    int PORTRAIT_PAGE_HEIGHT = 842;
	    int PORTRAIT_PAGE_WIDTH = 595;
		try{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			
			ResultSetMetaData rsmd = query.getMetaData();
			int counts = rsmd.getColumnCount();
			for(int x=1; x<=counts; x++){
		        header[x] = rsmd.getColumnName(x);
		    }
			
			FastReportBuilder drb = new FastReportBuilder();
			
			
			for (int i = 1; i <= counts; i++){
				int columnWidth = PORTRAIT_PAGE_WIDTH / counts;
				String key = header[i];
				AbstractColumn column = null;
		        try {
		            column = ColumnBuilder.getNew().setColumnProperty(key, String.class.getName())
				            .setTitle(key).build();
		        } catch (ColumnBuilderException ex) {
		            ex.printStackTrace();
		        }
		          drb.addColumn(column); 
			}
			
			drb.setIgnorePagination(true);
			
			DynamicReport dynamicReport = drb.build();
			JRResultSetDataSource rsds = new JRResultSetDataSource(query);
			JasperPrint jasperPrint = DynamicJasperHelper.generateJasperPrint(dynamicReport, new ClassicLayoutManager(), rsds);
				
			
			HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
			response.setContentType("text/plain");
			
			String filename = new StringBuffer(fileName).append(".txt").toString();
			response.addHeader("Content-Disposition", "attachment;filename="  + filename);
			
			ServletOutputStream output = response.getOutputStream();			
			//JasperExportManager.exportReportToPdfStream(jasperPrint, output);
			
			JRTextExporter exporterText = new JRTextExporter();  
			exporterText.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);  
			exporterText.setParameter(JRExporterParameter.OUTPUT_STREAM, output);  
			exporterText.setParameter(JRTextExporterParameter.PAGE_WIDTH, PORTRAIT_PAGE_WIDTH);
			exporterText.setParameter(JRTextExporterParameter.PAGE_HEIGHT, PORTRAIT_PAGE_HEIGHT);			
			exporterText.exportReport();  
			
			//facesContext.getApplication().getStateManager().saveView(facesContext);  
			facesContext.responseComplete();
			
			 
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			ReleaseConnection.close(con, cs, query);
		}
		return false;
	}
	
}
