package com.pakar.utils;

public class ValidationHelper {
	
    public static boolean isNullOrEmpty(String string) {
    	if(string != null && !string.trim().isEmpty())
            return false;
        return true;
    }
	
	public static Boolean emailFormat(String email) {
		if (!email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"))
			return false;
		return true;
	}

	public static String onlyNumeric(String validateThis) {
		String message = "SUCCESS";
		if (!validateThis.matches("^[0-9]+$")) {
			 message = "ERROR";
		}
		return message;
	} 
}