package com.pakar.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

public class DateHelper {
	
	public static String formatDateToString(Date date, String datePattern) {
		if(date != null) {
			DateFormat df = new SimpleDateFormat(datePattern);
			return df.format(date);
		} else
			return null;
	}
	
	public static Date formatStringToDate(String date, String datePattern) {
		if(date != null) {
			DateFormat df = new SimpleDateFormat(datePattern);
			Date d = new Date();
			try {
				d = df.parse(date);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return d;
		} else
			return null;
	}
	
	public static int calculateAge(LocalDate birthDate, LocalDate currentDate) {
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }
	
	public static LocalDate formatDateToLocalDate(Date date) {
		if(date != null) {
			LocalDate ld = null;
			try {
				ld = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return ld;
		} else
			return null;
	}
}